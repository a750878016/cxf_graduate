#include "stdio.h"
#include "usart2.h"

//PA2--TX
//PA3--RX
u8 auto_flag=0;	//允许本地自主控制

void Uart2_Init(u32 bound)
{
	//1.使能AFIO,GPIO和USART部件的时钟
	RCC->APB2ENR |=0x01<<0;
	RCC->APB2ENR |=0x01<<2;
	RCC->APB1ENR |=0x01<<17;
	
	//2.将USART Tx的GPIO配置为推挽复用模式,最大速度50MHz
	GPIOA->CRL &=~(0xF<<8);
	GPIOA->CRL |=(0xB<<8);
	
	//3.将USART Rx的GPIO配置为浮空输入模式
	GPIOA->CRL &=~(0xF<<12);
	GPIOA->CRL |=(0x01<<14);
	
	//4.配置USART参数
	USART2->BRR =36*1000000/bound;		//16*div=fck/bound
	USART2->CR1 &=~(0x01<<12);         	//数据长度为8位
	USART2->CR2 &=~(0x03<<12);    		//设置停止位为1位
	
	//nvic中设置uart2的优先级、使能uart2中断
	NVIC_SetPriority(USART2_IRQn,NVIC_EncodePriority(7-2,3,4));
	NVIC_EnableIRQ(USART2_IRQn);
	//接收缓冲区非空中断使能
	USART2->CR1 |=0X01<<5; 
	USART2->CR1 |=0X01<<4; //空闲中断
	
	//5.	使能接受、使能发送、开模块。
	USART2->CR1 |=0X01<<3;    //开串口的发送功能
	USART2->CR1 |=0X01<<2;    //开串口的接受功能
	USART2->CR1 |=0X01<<13;   //开串口1	
}

//串口2接收中断
TYPE_WIFI WifiRev={0};
void USART2_IRQHandler(void)
{
	u8 data;
	if(USART2->SR & (0X01<<5))  		//接收到一个数据
	{
		WifiRev.WifiRevBuf[WifiRev.Len++]=USART2->DR;	//数据+1
	}
	else if(USART2->SR & (0X01<<4))		//没有数据了，空闲状态
	{
		data=USART2->SR;
		data=USART2->DR;
		
		WifiRev.WifiRevBuf[WifiRev.Len]='\0';	//接收到"\0"
		WifiRev.Len=0;
		WifiRev.RevOver=1;						//接收标志为1
		printf("%s",WifiRev.WifiRevBuf);		//接收完成，串口一输出
		
		if(WifiRev.WifiRevBuf[2]==0x04)			//本节点
		{
			if(WifiRev.WifiRevBuf[4]==0x06&&WifiRev.WifiRevBuf[5]==0x01)
				auto_flag = 2;					//开启本地控制
			else if(WifiRev.WifiRevBuf[4]==0x06&&WifiRev.WifiRevBuf[5]==0x02)
				auto_flag = 1;					//关闭
			WifiRev.WifiRevBuf[3]=0x03;
			Uart2_SendString(WifiRev.WifiRevBuf);
		}		
	}	
}

//串口2发送字符串
void Uart2_SendString(u8 *p)
{
	while(*p!='\0')
	{
		while((USART2->SR & (0X01<<7))==0);  //等待发送缓冲区为空
		USART2->DR=*p++;
	}
}

//串口2发送字符串
void Uart2_SendHex(u8 *p)
{
	int flag=0;
	while(1)
	{
		if(*p==0x9f)
			flag=1;
		if(*p==0xf9&&flag==1)
			flag=2;
		if(flag==2)
			flag=3;
		while((USART2->SR & (0X01<<7))==0);  //等待发送缓冲区为空
		USART2->DR=*p++;
		if(flag==3)
		{
			flag=0;
			break;
		}
	}
}

#pragma import(__use_no_semihosting_swi) //取消半主机状态
