#ifndef _USART2_H_
#define _USART2_H_
#include "stm32f10x.h"

typedef struct{	
u8	WifiRevBuf[1024];	//接收缓冲区
u16 Len;				//长度
u8	RevOver;			//接收/发送完成标志
}TYPE_WIFI;

extern TYPE_WIFI WifiRev;		//接收WiFi模块发送过来的数据
extern u8 auto_flag;			//允许本地自主控制

void Uart2_Init(u32 bound);		//串口二初始化
void Uart2_SendString(u8 *p);	//数据发送
void Uart2_SendHex(u8 *p);

#define Wifi_SendString  Uart2_SendString


#endif
