#ifndef _ESP8266_H_
#define _ESP8266_H_
#include "stm32f10x.h"

#define TIMEOUT 1

/*函数功能：发送指定的AT指令，并且验证有没有期待的响应
  参数说明：pAt指向待发送的AT指令，pACK指向期待的响应信息
  返回值：0成功 其他失败*/
u8 Wifi_send(u8 *pAT,u8 *pACK);

void Connect_TCP(void);

#endif

