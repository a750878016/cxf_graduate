#include "esp8266.h"
#include "usart2.h"
#include "string.h"
#include "stdio.h"
#include "delay.h"

/*
函数功能：发送指定的AT指令，并且验证有没有期待的响应
参数说明：pAt指向待发送的AT指令，pACK指向期待的响应信息
返回值：0成功 其他失败
*/

u8 Wifi_send(u8 *pAT,u8 *pACK)
{
	u16 timeout=0;
	Wifi_SendString(pAT);//发送AT指令
	
	while(1)
	{
		while(!WifiRev.RevOver)		//0，表示还有数据
		{
			delay_ms(1);
			if(++timeout>10000)
				return TIMEOUT;		//超出等待时间，返回超时，1
		}							//接收标志为1，表示接收完成
		WifiRev.RevOver=0;
		
		//判断发送数据是否成功，成功返回0
		if( NULL!=strstr((const char *)WifiRev.WifiRevBuf,(const char *)pACK) )//字符串查找
			return 0;
	
	}
	
}

void Connect_TCP(void)
{
	//退出透传
	Wifi_SendString((u8 *)"+++" );
	delay_ms(500);	
	printf("WIFI CONNECT...\r\n");	
	//复位
	if( Wifi_send((u8 *)"AT+RST\r\n",(u8 *)"ready") )
	printf("AT+RST ERR\r\n");
	//设置 Wi-Fi 为 Station 模式并保存到 Flash
	if( Wifi_send((u8 *)"AT+CWMODE_DEF=1\r\n",(u8 *)"OK") )
	printf("AT+CWMODE_DEF=1 ERR\r\n");
	//wifi名，密码
	if( Wifi_send((u8 *)"AT+CWJAP_DEF=\"graduate_wifi\",\"12345678\"\r\n",(u8 *)"OK") )
	printf("WIFI CONNECT ERR\r\n");
	//连接服务器
	if( Wifi_send((u8 *)"AT+CIPSTART=\"TCP\",\"192.168.4.2\",8088\r\n",(u8 *)"OK") )
	printf("SERVER CONNECT ERR\r\n");
	//进入透传模式
	if( Wifi_send((u8 *)"AT+CIPMODE=1\r\n",(u8 *)"OK") )
	printf("AT+CIPMODE=1 ERR\r\n");
	//不指定长度发送
	if( Wifi_send((u8 *)"AT+CIPSEND\r\n",(u8 *)">") )
	printf("AT+CIPSEND ERR\r\n");
	printf("----------------\r\n");
}
