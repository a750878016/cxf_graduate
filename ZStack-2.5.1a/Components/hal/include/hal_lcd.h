/*******************************************************
 *                      OLED屏                         *
 * P1.2 -----> SCLK  时钟                              *
 * P1.3 -----> SDA   D1（MOSI） 数据                   *
 * P1.7 -----> RES   复位                              *
 * P0.0 -----> A0  H/L 命令数据选通端，H：数据，L:命令 *
 *******************************************************/

#ifndef HAL_LCD_H
#define HAL_LCD_H

#ifdef __cplusplus
extern "C"
{
#endif


#define OLED_1306
//#define OLED_1331  
  
 
/**************************************************************************************************
 *                                          INCLUDES
 **************************************************************************************************/
#include "hal_board.h"

/**************************************************************************************************
 *                                         CONSTANTS
 **************************************************************************************************/

/* These are used to specify which line the text will be printed */
#define HAL_LCD_LINE_1      0x01
#define HAL_LCD_LINE_2      0x02
/*
   This to support LCD with extended number of lines (more than 2).
   Don't use these if LCD doesn't support more than 2 lines
*/
#define HAL_LCD_LINE_3      0x03
#define HAL_LCD_LINE_4      0x04
#define HAL_LCD_LINE_5      0x05
#define HAL_LCD_LINE_6      0x06
#define HAL_LCD_LINE_7      0x07
#define HAL_LCD_LINE_8      0x08

/* Max number of chars on a single LCD line */
#define HAL_LCD_MAX_CHARS   16
#define HAL_LCD_MAX_BUFF    25
  

/**************************************************************************************************
 *                                          MACROS
 **************************************************************************************************/


/**************************************************************************************************
 *                                         TYPEDEFS
 **************************************************************************************************/


/**************************************************************************************************
 *                                     GLOBAL VARIABLES
 **************************************************************************************************/


/**************************************************************************************************
 *                                     FUNCTIONS - API
 **************************************************************************************************/

extern void HalLcdInit(void);   //oled初始化，系统调用

extern void HalLcdWriteString ( char *str, uint8 option);   //数据，用户可调用


/******************我不调用，不用管，系统定义的*************************/
/***********************************************************************/
extern void HalLcdWriteValue ( uint32 value, const uint8 radix, uint8 option);
extern void HalLcdWriteScreen( char *line1, char *line2 );
extern void HalLcdWriteStringValue( char *title, uint16 value, uint8 format, uint8 line );
extern void HalLcdWriteStringValueValue( char *title, uint16 value1, uint8 format1, uint16 value2, uint8 format2, uint8 line );
extern void HalLcdDisplayPercentBar( char *title, uint8 value );
/***********************************************************************/


#ifdef __cplusplus
}
#endif

#endif
