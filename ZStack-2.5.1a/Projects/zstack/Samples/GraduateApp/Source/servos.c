/***************************
 *      舵机(节点一)       *
 * P0.4 -----> DATA        *
 ***************************/

#include "ioCC2530.h"
#include "OnBoard.h"

#define PWM P0_4      //引脚选择

uint8 count = 0;        //计数值
uint8 c = 0;            //舵机角度选择,0:0度;1:45度;2:90度;3:135度;4:180度;


void InitT3(void);      //初始化定时器,
void InitServos(void);  //初始化舵机
void Servos_Stop(void);

//初始化定时器,
void InitT3(void)
{ 
  T3CTL |= 0x08 ;       //开溢出中断  
  T3IE = 1;             //开T3中断
  T3CTL |= 0xE0;        //128分频,128/32000*N=0.5ms,N=125 
  
  T3CTL |= 0x02;        //模计数  
  T3CC0 = 0x7d;         //设置计数值,32MHZ下128分频,计数125下就是0.5ms
  T3CCTL0 |= 0x04;      //开启比较模式，不然模模式下溢出不产生中断 
  T3CCTL0 |= 0x40;      //中断使能
  T3CTL |= 0x10;        //启动   
  EA = 1;               //开总中断
}


//初始化舵机
void InitServos(void)
{
  P0DIR |= 0x10;            //P0.4为输出模式
  PWM = 0;
  InitT3();                 //初始化定时器  
}


//定时器T3中断处理函数
#pragma vector = T3_VECTOR 
__interrupt void T3_ISR(void) 
{ 
  IRCON = 0x00;     //清中断标志, 也可由硬件自动完成 
    
  if(count<=c)      //进入定时器的时间和高低电平临界值比较
  {
    PWM=1;          //输出PWM波形中的高电平
  }
  else
  {
    PWM=0;
  }
  count++;
  if(count>=40)     //计数40次，每次0.5ms，总共达到20ms周期后清零
  {
    count=0;
  }
}

void Servos_Start(uint8 n)
{
  c = (uint8)((n)/45);    //设置角度，由于不能接收0x00,所以所有角度+1发送
}

void Servos_Stop(void)
{
  T3CTL &= ~0x10;       //关闭
  T3IE = 0;             //关T3中断
  T3CCTL0 &= ~0x40;     //中断失能
  PWM = 0;
}