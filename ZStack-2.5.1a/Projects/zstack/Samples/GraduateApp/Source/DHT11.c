/***************************
 *    温湿度模块(节点1)    *
 * P0.7 -----> DATA        *
 ***************************/

#include <ioCC2530.h>
#include "OnBoard.h"
#include "delay.h"
#include "DHT11.h"

//要修改的地方
#define DATA_PIN P0_7
#define DATA_PIN_INPUT  (P0DIR &= ~0x80)
#define DATA_PIN_OUTPUT (P0DIR |= 0x80)

void COM(void);
void DHT11(void);

//温湿度定义
uchar ucharFLAG,uchartemp;
uchar shidu,wendu;
uchar ucharT_data_H,ucharT_data_L,ucharRH_data_H,ucharRH_data_L,ucharcheckdata;
uchar ucharT_data_H_temp,ucharT_data_L_temp,ucharRH_data_H_temp,ucharRH_data_L_temp,ucharcheckdata_temp;
uchar ucharcomdata;

//温湿度传感，读一字节
void COM(void)    // 温湿写入
{     
    uchar i;         
    for(i=0;i<8;i++)    //循环8次为一个字节 
    {
        ucharFLAG=2; 
        while((!DATA_PIN)&&ucharFLAG++);    //检测到高电平，跳出
        Delay_10us();                       //等待30us
        Delay_10us();
        Delay_10us();
        uchartemp=0;
        if(DATA_PIN)uchartemp=1;    //30u后还是高电平，读1，否者默认为0
        ucharFLAG=2;
        while((DATA_PIN)&&ucharFLAG++);     //等待下一bit开始 
        if(ucharFLAG==1)break;    
        ucharcomdata<<=1;           //右移1bit
        ucharcomdata|=uchartemp;    
    }    
}

void DHT11(void)   //温湿传感启动
{
    DATA_PIN=0;     //主机拉低
    Delay_ms(19);   //>18MS
    DATA_PIN=1;     //主机拉高
    DATA_PIN_INPUT; //输入模式
    Delay_10us();   //等待40us
    Delay_10us();                        
    Delay_10us();
    Delay_10us();  
    if(!DATA_PIN)   //DHT11发送低电平
    {
        ucharFLAG=2; 
        while((!DATA_PIN)&&ucharFLAG++);    //DHT11发送高电平退出
        ucharFLAG=2;
        while((DATA_PIN)&&ucharFLAG++);     //DHT11发送低电平退出，表示将发送8bit数据
        COM();                              
        ucharRH_data_H_temp=ucharcomdata;   //湿度高位
        COM();
        ucharRH_data_L_temp=ucharcomdata;   //湿度低位
        COM();
        ucharT_data_H_temp=ucharcomdata;    //温度高位
        COM();
        ucharT_data_L_temp=ucharcomdata;    //温度低位
        COM();
        ucharcheckdata_temp=ucharcomdata;   //校验和
        DATA_PIN=1; 
        uchartemp=(ucharT_data_H_temp+ucharT_data_L_temp+ucharRH_data_H_temp+ucharRH_data_L_temp);
        if(uchartemp==ucharcheckdata_temp)  //数据无误
        {
            ucharRH_data_H=ucharRH_data_H_temp;
            ucharRH_data_L=ucharRH_data_L_temp;
            ucharT_data_H=ucharT_data_H_temp;
            ucharT_data_L=ucharT_data_L_temp;
            ucharcheckdata=ucharcheckdata_temp;
        }

        wendu=ucharT_data_H;                //返回温度
        shidu=ucharRH_data_H;               //返回湿度
    } 
    else //没用成功读取，返回0
    {
        shidu=0;
        wendu=0;
        
    } 
    
    DATA_PIN_OUTPUT; //输出
}
