#include <stdio.h>
#include <string.h>
#include "AF.h"
#include "OnBoard.h"
#include "OSAL_Tasks.h"
#include "GraduateApp.h"
#include "ZDApp.h"
#include "ZDObject.h"
#include "ZDProfile.h"
#include "string.h"

/* 用到的外设 */
#include "hal_drivers.h"
#include "hal_key.h"
#if defined ( LCD_SUPPORTED )
  #include "hal_lcd.h"
#endif
#include "hal_led.h"
#include "hal_uart.h"
#include "dht11.h"
#include "hal_adc.h"

/* 自定义头文件 */
#include "delay.h"
#include "DHT11.h"      //温湿度
#include "light.h"      //光强
#include "MQ2.h"        //可燃气体
#include "rain.h"       //雨滴
#include "buzzer.h"     //蜂鸣器
#include "motor.h"      //步进电机
#include "servos.h"     //舵机
#include "relays.h"     //继电器

/*节点宏,编译哪种，就把那种取消注释*/
//#define TERMINAL_1  1
//#define TERMINAL_2  2
#define COORDINATOR 3

#if !defined( GRADUATE_APP_BAUD )   //波特率
  #define GRADUATE_APP_BAUD  HAL_UART_BR_115200
#endif

/*此列表应填充特定于应用程序的群集 ID*/
/*一个用于采集数据和反馈状态的数据/状态簇，一个用于控制设备的控制簇*/
const cId_t GraduateApp_ClusterList[GRADUATE_MAX_CLUSTERS] =
{
  GRADUATEAPP_DATA_CLUSTERID,       //数据/状态簇
  GRADUATEAPP_CONTROL_CLUSTERID,    //控制簇
};

/*定义简单设备描述符，可以理解为这个设备的标识*/
const SimpleDescriptionFormat_t GraduateApp_SimpleDesc =
{
  GRADUATEAPP_ENDPOINT,              //  int   端口号;
  GRADUATEAPP_PROFID,                //  uint16 应用规范ID;
  GRADUATEAPP_DEVICEID,              //  uint16 设备ID;
  GRADUATEAPP_DEVICE_VERSION,        //  int   应用设备版本号:4;
  GRADUATEAPP_FLAGS,                 //  int   标识:4;
  GRADUATE_MAX_CLUSTERS,             //  byte  最大簇;
  (cId_t *)GraduateApp_ClusterList,  //  byte *pAppInClusterList;
  GRADUATE_MAX_CLUSTERS,             //  byte  最大簇;
  (cId_t *)GraduateApp_ClusterList   //  byte *pAppOutClusterList;
};

endPointDesc_t GraduateApp_epDesc;   //端点描述符
byte GraduateApp_TaskID;
devStates_t GraduateApp_NwkState;    //终端设备状态
byte GraduateApp_TransID;

afAddrType_t GraduateApp_Periodic_DstAddr; //广播
afAddrType_t GraduateApp_P2P_DstAddr;      //点播

unsigned char uartbuf[64];         //存放串口读取的数据
unsigned char sendbuf[64];         //存放将要串口发送的数据
unsigned char data[64];            //用于存放将要无线发送的数据
unsigned char rcvdata[64];         //用于存放无线发送过来的数据
unsigned char lcdbuf[64];          //lcd字符串

unsigned char rain[16];            //存放传感器数据，用于lcd显示
unsigned char HT[16];
unsigned char MQ2[16];
unsigned char light[16];
unsigned char mun=3;               //显示计数
unsigned char flag=0;              //网络状态改变，设备只初始化一次

static void rxCB(uint8 port,uint8 event);   //串口接收回调函数
void SampleApp_CallBack(uint8 port, uint8 event); 
    
//函数声明
static void GraduateApp_Send_P2P_Message( unsigned char * data );      //点播
static void GraduateApp_Send_Broadcast_Message( unsigned char * data );//广播
static void GraduateApp_ProcessMSGCmd( afIncomingMSGPacket_t *pkt );   //事件处理函数

static void GraduateApp_Coordinator(void);                  //协调器控制任务
static void GraduateApp_Node_1(unsigned char data[]);       //节点一控制任务
static void GraduateApp_Node_2(unsigned char data[]);       //节点二控制任务


//自定义节点初始化
void GraduateApp_Init( byte task_id )
{
  GraduateApp_TaskID = task_id;
  GraduateApp_NwkState = DEV_INIT;      //已初始化，不连接任何内容
  GraduateApp_TransID = 0;
  GraduateApp_epDesc.endPoint = GRADUATEAPP_ENDPOINT;       //端口号
  GraduateApp_epDesc.task_id = &GraduateApp_TaskID;
  GraduateApp_epDesc.simpleDesc = (SimpleDescriptionFormat_t *)&GraduateApp_SimpleDesc;
  GraduateApp_epDesc.latencyReq = noLatencyReqs;            //必须用noLatencyReqs来填充
  afRegister(&GraduateApp_epDesc);      //注册一个应用的端点描述符
  
  /*****配置串口*****/
  halUARTCfg_t uartConfig;              //定义halUARTCfg_t类型的变量
  uartConfig.configured = true;         
  uartConfig.baudRate = GRADUATE_APP_BAUD; //波特率
  uartConfig.flowControl = false;       //流控制
  uartConfig.callBackFunc = rxCB;       //回调函数
  HalUARTOpen(0,&uartConfig);           //初始化串口
  
  //广播
  GraduateApp_Periodic_DstAddr.addrMode = (afAddrMode_t)AddrBroadcast;  //广播
  GraduateApp_Periodic_DstAddr.endPoint = GRADUATEAPP_ENDPOINT;         //11
  GraduateApp_Periodic_DstAddr.addr.shortAddr = 0xFFFF;                 
  
  //点播
  GraduateApp_P2P_DstAddr.addrMode = (afAddrMode_t)Addr16Bit;   //点播 
  GraduateApp_P2P_DstAddr.endPoint = GRADUATEAPP_ENDPOINT;      //11
  GraduateApp_P2P_DstAddr.addr.shortAddr = 0x0000;              //发给协调器
}

static void rxCB(uint8 port,uint8 event)    //串口接收回调函数
{
#ifdef COORDINATOR      //只有协调器才接收串口回调
  osal_memset(uartbuf,0,13);
  HalUARTRead(0,uartbuf,13);
  if(uartbuf[2]==0x03)  //协调器自身
  {
    GraduateApp_Coordinator();
  }
  if(uartbuf[2]==0x01 || uartbuf[2]==0x02)         //转发给节点
  {
    GraduateApp_Send_Broadcast_Message(uartbuf);   //广播
  }
#endif
}

//串口读取回调函数，实际没用到
void SampleApp_CallBack(uint8 port, uint8 event)
{
  (void)port;
}

//事件函数
UINT16 GraduateApp_ProcessEvent(byte task_id,UINT16 events)
{
  afIncomingMSGPacket_t *MSGpkt;
  if(events & SYS_EVENT_MSG)    //全局系统事件
  {
    MSGpkt = (afIncomingMSGPacket_t *)osal_msg_receive(GraduateApp_TaskID);
    while(MSGpkt)
    {
      switch(MSGpkt->hdr.event) //OSAL 消息标头
      {
      case ZDO_STATE_CHANGE:    //当设备角色发生转变后才会触发
        GraduateApp_NwkState = (devStates_t)(MSGpkt->hdr.status);
        if(GraduateApp_NwkState == DEV_ZB_COORD) //改变为协调器
        {
          if(flag==0)
          {
            Buzzer_Init();    //蜂鸣初始化
            Init_Relays_1();  //继电器初始化,与其他节点不同
            P1DIR |= 0x02;    //LED2初始化
            P1_1 = 0;
            //任务1：3秒采集一次光照强度
            osal_start_timerEx(GraduateApp_TaskID,COORDINATORAPP_CAPTURE_LIGHTING,3000);
            //任务4：轮流显示各个节点的数据
            osal_start_timerEx(GraduateApp_TaskID,COORDINATORAPP_LCD_SHOWN,4000);
            flag=1;
          }
          
        }
        //否则为终端，终端每5秒向协调器发送自己的数据
        if(GraduateApp_NwkState==DEV_END_DEVICE)
        {
#ifdef TERMINAL_1           //终端节点1
          if(flag==0)
          {
            Init_Relays();    //继电器初始化 
            InitServos();     //初始化舵机
            //任务2：节点一3秒采集一次数据并上传
            osal_start_timerEx(GraduateApp_TaskID,NODEAPP_1_CAPTURE_RAIN,3000);
            flag=1;
          }

#endif
          
#ifdef TERMINAL_2           //终端节点2
          if(flag==0)
          {
            initSensorPort(); //步进电机初始化
            Init_Relays();    //继电器初始化
            //任务3：节点二5秒采集一次数据并上传
            osal_start_timerEx(GraduateApp_TaskID,NODEAPP_2_CAPTURE_MQ2,3000);
            flag=1;
          }
#endif
        }
        break;
        
      case AF_INCOMING_MSG_CMD:     //无线数据
        GraduateApp_ProcessMSGCmd( MSGpkt );
        break;
        
      default:
        break;
      }
      osal_msg_deallocate((uint8*)MSGpkt);
      MSGpkt=(afIncomingMSGPacket_t*)osal_msg_receive(GraduateApp_TaskID);
    }
    return(events^SYS_EVENT_MSG);
  }
#ifdef COORDINATOR 
  /*协调器采集光照*/
  if(events&COORDINATORAPP_CAPTURE_LIGHTING)
  {
    HalLedBlink(HAL_LED_1,4,50,(1000/4));
    osal_memset(data,0,64);
    data[0] = 0xa5;     //帧头
    data[1] = 0x5a;
    data[2] = 0x03;     //节点
    data[3] = 0x02;     //数据   
    data[4] = 0x04;     //设备/*光照*/
    uchar Light = GetLight();
    uchar LightH_L = GetlightH_L();
    data[5] = Light+1;  //不传0x00
    data[6] = LightH_L;
    data[7] = 0xff;     //补充
    data[8] = 0xff;
    data[9] = 0x0e;
    data[10] = 0xff;    //校验位
    data[11] = 0x0d;
    data[12] = 0x0a;
    HalUARTWrite(0,data,13); //串口发送   
    osal_memset(light,0,16); //用于OLED显示
    osal_memcpy(light,data,osal_strlen(data)+1);   
    //任务1：3秒采集一次光照强度
    osal_start_timerEx(GraduateApp_TaskID,COORDINATORAPP_CAPTURE_LIGHTING,3000);
    return(events ^ COORDINATORAPP_CAPTURE_LIGHTING);
  }
  if(events&COORDINATORAPP_LCD_SHOWN)   //OLED屏显示
  {
    if(mun==3)
    {
      HalLcdWriteString("Coordinator",1);
      HalLcdWriteString("node 3",2);
      sprintf(lcdbuf,"光强:%d",light[5]-1);
      HalLcdWriteString(lcdbuf,3);
      HalLcdWriteString(" ",4); 
      mun=0;
    }
    if(mun==1)
    {
      HalLcdWriteString("Terminal",1);
      HalLcdWriteString("node 1",2);
      sprintf(lcdbuf,"雨滴:%d",rain[5]-1);
      HalLcdWriteString(lcdbuf,3);
      sprintf(lcdbuf,"温度:%d,湿度:%d",HT[5],HT[6]);
      HalLcdWriteString(lcdbuf,4); 
    }
    if(mun==2)
    {
      HalLcdWriteString("Terminal",1);
      HalLcdWriteString("node 2",2);
      sprintf(lcdbuf,"可燃气体:%d",MQ2[5]);
      HalLcdWriteString(lcdbuf,3);
      HalLcdWriteString(" ",4);   
    }
    mun++;
    osal_start_timerEx(GraduateApp_TaskID,COORDINATORAPP_LCD_SHOWN,4000);
    return(events ^ COORDINATORAPP_LCD_SHOWN);
  }
#endif
  
#ifdef TERMINAL_1      
  /*节点一采集数据_雨滴*/
  if(events&NODEAPP_1_CAPTURE_RAIN)
  {
    HalLcdWriteString("Terminal",1);
    HalLcdWriteString("node 1",2);
    
    HalLedBlink(HAL_LED_1,4,50,(1000/4));
    osal_memset(data,0,64);
    data[0] = 0xa5;     //帧头
    data[1] = 0x5a;
    data[2] = 0x01;     //节点
    data[3] = 0x02;     //数据   
    /*雨滴*/  
    data[4] = 0x01;     //设备码
    uchar rain = GetRain();   
    uchar rainH_L = GetRainH_L();
    
    data[5] = rain+1;
    data[6] = rainH_L;
    data[7] = 0xff;     //补充
    data[8] = 0xff;
    data[9] = 0x0e;
    data[10] = 0xff;    //校验位
    data[11] = 0x0d;
    data[12] = 0x0a;
    GraduateApp_Send_P2P_Message(data); //发送
    
    sprintf(uartbuf,"雨滴:%d",rain);
    HalLcdWriteString(uartbuf,3);
    
    osal_start_timerEx(GraduateApp_TaskID,NODEAPP_1_CAPTURE_DHT11,1500);
    return(events ^ NODEAPP_1_CAPTURE_RAIN);
  }
  /*节点一采集数据_温湿度*/
  if(events&NODEAPP_1_CAPTURE_DHT11)
  {
    HalLcdWriteString("Terminal",1);
    HalLcdWriteString("node 1",2);
    
    HalLedBlink(HAL_LED_1,4,50,(1000/4));
    osal_memset(data,0,64);
    data[0] = 0xa5;     //帧头
    data[1] = 0x5a;
    data[2] = 0x01;     //节点
    data[3] = 0x02;     //数据  
  
    /*温湿度*/
    data[4] = 0x02;     //设备码
    DHT11();
    sprintf(uartbuf,"温度:%dC,湿度:%dRH",wendu,shidu);
    data[5] = wendu;
    data[6] = shidu;
    data[7] = 0xff;     //补充
    data[8] = 0xff;
    data[9] = 0x0e;
    data[10] = 0xff;    //校验位
    data[11] = 0x0d;
    data[12] = 0x0a;
    GraduateApp_Send_P2P_Message(data); //发送
    HalLcdWriteString(uartbuf,4); 
    
    osal_start_timerEx(GraduateApp_TaskID,NODEAPP_1_CAPTURE_RAIN,1500);
    return(events ^ NODEAPP_1_CAPTURE_DHT11);
  }
#endif
  
#ifdef TERMINAL_2      
  /*节点二采集数据_气体*/
  if(events&NODEAPP_2_CAPTURE_MQ2)
  {
    HalLcdWriteString("Terminal",1);
    HalLcdWriteString("node 2",2);   
    HalLedBlink(HAL_LED_1,4,50,(1000/4));
    osal_memset(data,0,64);
    data[0] = 0xa5;     //帧头
    data[1] = 0x5a;
    data[2] = 0x02;     //节点
    data[3] = 0x02;     //数据
    data[4] = 0x03;     //设备码/*MQ2*/
    uchar Mq2 = GetMq2();
    uchar Mq2H_L = GetMq2H_L();
    data[5] = Mq2;      //数据1
    data[6] = Mq2H_L;   //数据2
    data[7] = 0xff;     //补充
    data[8] = 0xff;
    data[9] = 0x0e;
    data[10] = 0xff;    //校验位
    data[11] = 0x0d;
    data[12] = 0x0a;
    GraduateApp_Send_P2P_Message(data); //发送   
    sprintf(uartbuf,"可燃气体:%d",Mq2);
    HalLcdWriteString(uartbuf,4);    
    osal_start_timerEx(GraduateApp_TaskID,NODEAPP_2_CAPTURE_MQ2,3000);
    return(events ^ NODEAPP_2_CAPTURE_MQ2);
  }
#endif
  return 0;
}

//点播，一般用于向协调器发送数据
void GraduateApp_Send_P2P_Message( unsigned char * data )
{  
  //无线发送到协调器
  if ( AF_DataRequest( &GraduateApp_P2P_DstAddr, &GraduateApp_epDesc,
                       GRADUATEAPP_DATA_CLUSTERID, //数据簇
                       osal_strlen(data)+1,       //长度
                       data,                    //数据
                       &GraduateApp_TransID,    //序号  
                       AF_DISCV_ROUTE,          //默认
                       AF_DEFAULT_RADIUS ) == afStatus_SUCCESS )
  {
  }
  else
  {
    // Error occurred in request to send.
  }
}

//广播播，一般是协调器向终端发送控制命令
void GraduateApp_Send_Broadcast_Message( unsigned char * data )
{  
  
  //广播
  if ( AF_DataRequest( &GraduateApp_Periodic_DstAddr, &GraduateApp_epDesc,
                       GRADUATEAPP_CONTROL_CLUSTERID, //控制簇
                       osal_strlen(data)+1,       //长度
                       data,                    //数据
                       &GraduateApp_TransID,    //序号  
                       AF_DISCV_ROUTE,          //默认
                       AF_DEFAULT_RADIUS ) == afStatus_SUCCESS )
  {
  }
  else
  {
    // Error occurred in request to send.
  }
}

void GraduateApp_ProcessMSGCmd( afIncomingMSGPacket_t *pkt )
{
  switch ( pkt->clusterId )
  {
  case GRADUATEAPP_DATA_CLUSTERID:      //数据/状态簇,协调器接收终端传来的数据
    //协调器直接将无线数据通过串口发送
    osal_memset(sendbuf,0,16);
    osal_memcpy(sendbuf,pkt->cmd.Data,osal_strlen(pkt->cmd.Data)+1);
    HalUARTWrite(0,sendbuf,13);
    
    if(sendbuf[0]==0xa5 && sendbuf[1]==0x5a && sendbuf[3]==0x02)//数据帧
    {  
      if(sendbuf[2]==0x01)
      {
        if(sendbuf[4]==0x01)    //雨滴
        {
          osal_memset(rain,0,16);
          osal_memcpy(rain,sendbuf,osal_strlen(sendbuf)+1);
        }
        if(sendbuf[4]==0x02)    //DHT11
        {
          osal_memset(HT,0,16);
          osal_memcpy(HT,sendbuf,osal_strlen(sendbuf)+1);
        }
      }
      if(sendbuf[2]==0x02)      //MQ2
      {
        osal_memset(MQ2,0,16);
        osal_memcpy(MQ2,sendbuf,osal_strlen(sendbuf)+1);
      }
    }
    break;

  case GRADUATEAPP_CONTROL_CLUSTERID:   //控制簇,终端接收协调器发送的命令
    osal_memset(rcvdata,0,16);
    osal_memcpy(rcvdata,pkt->cmd.Data,osal_strlen(pkt->cmd.Data)+1);
#ifdef  TERMINAL_1
    if(rcvdata[2]== 0x01)   //第2字节是节点类型//节点一
      GraduateApp_Node_1(rcvdata);
#endif
      
#ifdef  TERMINAL_2    
    if(rcvdata[2]== 0x02)             //节点二
      GraduateApp_Node_2(rcvdata);
#endif
    
    break;
  
  default:
    break;
  }
}

//协调器是从串口接收的数据帧
void GraduateApp_Coordinator(void)
{
  if(uartbuf[3]==0x01) //0x01为控制，0x02为数据
  {
   switch(uartbuf[4]) //第四字节为设备码
    { 
    case 0x01:          //热水器，继电器
      if(uartbuf[5]==0x01)
        Relays_On_1();    //开     
      else if(uartbuf[5]==0x02)
        Relays_Off_1();   //关
 
      uartbuf[3] = 0x03;            //反馈型数据  
      HalUARTWrite(0,uartbuf,13);   //串口发送回去    
      break;
      
    case 0x04:          //蜂鸣器
      if(uartbuf[5]==0x01) 
        Buzzer_ON();    //开
      else if(uartbuf[5]==0x02)
        Buzzer_OFF();   //关
      
      Delay_ms(300);
      uartbuf[3] = 0x03;            //反馈型数据  
      HalUARTWrite(0,uartbuf,13);   //串口发送回去    
      break;
      
    case 0x05:          //led
      if(uartbuf[5]==0x01) 
         P1_1 = 1;      //开,高电平驱动
      else if(uartbuf[5]==0x02)
         P1_1 = 0;      //关
      
      Delay_ms(300);
      uartbuf[3] = 0x03;            //反馈型数据  
      HalUARTWrite(0,uartbuf,13);   //串口发送回去  
    default:
      break;
    }
  }
}

//节点是无线接收的数据
void GraduateApp_Node_1(unsigned char data[])   //节点一控制任务
{
  if(data[3]==0x01)     //0x01为控制，0x02为数据
  {
    switch(data[4])     //第四字节为设备码
    {
    case 0x01:          //继电器，洗衣机
      if(data[5]==0x01)
        Relays_On();    //开
      else if(data[5]==0x02)
        Relays_Off();   //关   
      
      data[3] = 0x03;   //反馈型数据
      GraduateApp_Send_P2P_Message(data); //发送    
      break;
    case 0x02:              //舵机，门
      if(data[5]==0x02)
        Servos_Start(0);    //关
      else if(data[5]==0x01)
        Servos_Start(90);   //开
      
      data[3] = 0x03;       //反馈型数据  
      GraduateApp_Send_P2P_Message(data); //发送
      break;
    default:
      break;
    }
  }
}
void GraduateApp_Node_2(unsigned char data[])   //节点二控制任务
{
  if(data[3]==0x01) //0x01为控制，0x02为数据
  {
    switch(data[4]) //第四字节为设备码
    {
    case 0x01:          //继电器，空调
      if(data[5]==0x01)
        Relays_On();    //开
      else if(data[5]==0x02)
        Relays_Off();   //关
      
      data[3] = 0x03;   //反馈型数据
      GraduateApp_Send_P2P_Message(data); //发送    
      break;
      
    case 0x03:              //步进电机，窗户
      if(data[5]==0x01)     //正转,打开
        motor_up();         //2圈
      else if(data[5]==0x02)//反转，关闭
        motor_down();       //2圈
      
      data[3] = 0x03;       //反馈型数据
      GraduateApp_Send_P2P_Message(data); //发送    
      break;
    default:
      break;
    }
  }
}
