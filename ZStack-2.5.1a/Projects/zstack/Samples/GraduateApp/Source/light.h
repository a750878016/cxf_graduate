/***************************
 *        光敏电阻         *
 * P0.6 -----> AO 模拟引脚 *
 * p1.5 -----> DO 数字引脚 *
 ***************************/

#ifndef __LIGHT_H__
#define __LIGHT_H__

extern uint8 GetLight(void);        //获取光强百分比

extern uint8 GetlightH_L(void);     //获取光敏数字引脚信号
                                    //超出阈值，返回1；否则返回0
#endif