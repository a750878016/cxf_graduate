/***************************
 *        光敏电阻         *
 * P0.6 -----> AO 模拟引脚 *
 * p1.5 -----> DO 数字引脚 *
 ***************************/

#ifndef __MQ2_H__
#define __MQ2_H__

extern uint8 GetMq2(void);        //获取可燃气体百分比

extern uint8 GetMq2H_L(void);     //获取MQ2数字引脚信号
                                  //超出阈值，返回1；否则返回0
#endif