#include <ioCC2530.h>
#include "OnBoard.h"
#include "hal_adc.h"
#include "light.h"

//要修改的地方
#define DATA_PIN P1_5                       //数字引脚
#define DATA_PIN_INPUT  (P1DIR &= ~0x20)    //输入模式

uint8 GetLight(void)
{
  uint8 temp = 0;   //百分比的整数值
  float vol = 0.0;  //adc采样电压
  //P0_6引脚,adc第6通道,14位有效
  uint16 adc=HalAdcRead(HAL_ADC_CHANNEL_6,HAL_ADC_RESOLUTION_14);
  
  if(adc>8192)      //2^13,最大采样值8192(因为最高位是符号位)
  {
    return 0;
  }
  adc = 8192-adc;   //反相一下，因为低湿度时AO口输出较高电平
                    //高湿度时AO口输出较低电平 
  
  vol=(float)((float)adc)/8192.0;   //转化为百分比 
  
  temp=(uint8)(vol*100);     //取百分比两位数字
  
  return temp;
}

uint8 GetlightH_L(void) //获取光敏电阻的数字引脚信号
{
  DATA_PIN_INPUT;   //输入模式
  if(DATA_PIN)      //低于阈值，返回2；否则返回1
    return 2;
  return 1;
}