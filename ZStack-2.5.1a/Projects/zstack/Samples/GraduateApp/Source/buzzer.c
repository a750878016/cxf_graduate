#include <ioCC2530.h>
#include "buzzer.h"

#define DATA_PIN P0_7       //定义P0.7口为蜂鸣器的控制口


void Buzzer_Init(void)
{
  P0DIR |= 0x80;            //P0.7定义为输出口 
  DATA_PIN = 0;
}

void Buzzer_ON(void)        //开
{
  DATA_PIN = 1;
}

void Buzzer_OFF(void)       //关
{
  
  DATA_PIN = 0;
}
