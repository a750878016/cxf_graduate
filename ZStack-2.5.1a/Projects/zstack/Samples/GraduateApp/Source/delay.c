#include "OnBoard.h"
#include "delay.h"

typedef unsigned char uchar;
typedef unsigned int  uint;

void Delay_us(void);
void Delay_10us(void);
void Delay_ms(uint Time);

//延时函数
void Delay_us(void) //1 us延时
{
    MicroWait(1);   
}

void Delay_10us(void) //10 us延时
{
   MicroWait(10);
}

void Delay_ms(uint Time)//n ms延时
{
  unsigned char i;
  while(Time--)
  {
    for(i=0;i<100;i++)
     Delay_10us();
  }
}