/***************************
 *     雨滴模块(节点1)     *
 * P0.6 -----> AO 模拟引脚 *
 * p1.5 -----> DO 数字引脚 *
 ***************************/

#include <ioCC2530.h>
#include "OnBoard.h"
#include "hal_adc.h"
#include "rain.h"

//要修改的地方
#define DATA_PIN P1_5                       //数字引脚
#define DATA_PIN_INPUT  (P1DIR &= ~0x20)    //输入模式

//读取雨水的浓度
uint8 GetRain(void)
{
  uint16 adc= 0;
  float vol=0.0; //adc采样电压  
  uint8 soil_hum=0;
  adc=HalAdcRead(HAL_ADC_CHANNEL_6, HAL_ADC_RESOLUTION_14); //雨水 ADC 采样值 P06口
  
  //最大采样值8192(因为最高位是符号位)
  if(adc>=8192)
  {
    return 0;
  }
  
  adc=8192-adc;//反相一下，因为低湿度时AO口输出较高电平
     //          高湿度时AO口输出较低电平   
  
  //转化为百分比
  vol=(float)((float)adc)/8192.0;
  
  //雨水百分比的整数值
  soil_hum=(uint8)(vol*100);
  return soil_hum;
}

uint8 GetRainH_L(void)
{
  DATA_PIN_INPUT;   //输入模式
  if(DATA_PIN)      //低于阈值，即指示灯亮，返回2；否则返回1
    return 2;
  return 1;
}