//2023/5/23，减少圈数功能，设为固定2圈
#include <ioCC2530.h>
#include "delay.h"
#include "motor.h"
#include <string.h>

#define DELAY_TIME  1 //转一个角度停留的时间,可调节转速
                       //调整转速，这个值太小电机会失步

//引脚宏定义
#define PIN_A   P0_4 
#define PIN_B   P2_0
#define PIN_C   P1_6
#define PIN_D   P0_7

//**********************正向旋转相序表*****************************
unsigned char FFW[8]={0x80,0xc0,0x40,0x60,0x20,0x30,0x10,0x90}; //八拍
//**********************反向旋转相序表*****************************
unsigned char REV[8]={0x90,0x10,0x30,0x20,0x60,0x40,0xc0,0x80};

unsigned char FFW_4[4]={0x90,0xc0,0x60,0x30}; //双四拍
unsigned char REV_4[4]={0x30,0x60,0xc0,0x90};

unsigned char flag_1=0;


void initSensorPort(void)   //步进电机引脚初始化
{
  P0DIR |= 0x90;        //P0_4、P0_7定义为输出
  P1DIR |= 0x40;        //P1_6输出
  P2DIR |= 0x01;        //P2_0输出
  PIN_A = 1;
  PIN_B = 1;
  PIN_C = 1;
  PIN_D = 1;
  Delay_ms(500);
  PIN_A = 0;
  PIN_B = 0;
  PIN_C = 0;
  PIN_D = 0;
}

/*******************步进电机正转*********************/
void motor_up(void)      //参数n:表示正转的圈数
{
  if(flag_1 == 1) //已经开了，直接跳过
    return;
  flag_1 = 1;     //否则，状态为开
  unsigned char i;
  unsigned int j;
  unsigned int n=2;      //2圈
  int movie_count=(int)(8*64*n);    //步进电机减速比是 64:1
  for (j=0; j<movie_count; j++)
  {
    for (i=0; i<8; i++)
    {
      PIN_D = ((FFW[i]&0x80)==0x80);
      PIN_C = ((FFW[i]&0x40)==0x40);
      PIN_B = ((FFW[i]&0x20)==0x20);
      PIN_A = ((FFW[i]&0x10)==0x10);
      
      Delay_ms(DELAY_TIME); //转一个角度停留的时间,可调节转速
                            //调整转速，这个值太小电机会失步
    }
  }
  PIN_A = 0;
  PIN_B = 0;
  PIN_C = 0;
  PIN_D = 0;
}


/*******************步进电机反转*********************/
void motor_down(void)
{
  if(flag_1 == 0) //已经关了，直接跳过
    return;
  flag_1 = 0;     //否则，状态为关
  unsigned char i;
  unsigned int j;
  unsigned int n=2;     //2圈
  int movie_count=(int)(8*64*n);    //步进电机减速比是 64:1
  for (j=0; j<movie_count; j++)
  {
    for (i=0; i<8; i++)
    {
      PIN_D = ((REV[i]&0x80)==0x80);
      PIN_C = ((REV[i]&0x40)==0x40);
      PIN_B = ((REV[i]&0x20)==0x20);
      PIN_A = ((REV[i]&0x10)==0x10);
      
      Delay_ms(DELAY_TIME);     //转一个角度停留的时间,可调节转速
                                //调整转速，这个值太小电机会失步
    }
  }
  PIN_A = 0;
  PIN_B = 0;
  PIN_C = 0;
  PIN_D = 0;
}
