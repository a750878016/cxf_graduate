#ifndef DELAY_H
#define DELAY_H


extern void Delay_us(void);             //us
extern void Delay_10us(void);           //10us
extern void Delay_ms(unsigned int xms);	//ms

#endif