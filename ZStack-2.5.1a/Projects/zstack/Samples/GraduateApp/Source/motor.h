#ifndef __MOTOR_H__
#define __MOTOR_H__

extern void initSensorPort(void);   //初始化
extern void motor_up(void);      //正转
extern void motor_down(void);    //反转

#endif