/***************************
 *  继电器(所以节点一样)   *
 * P0.5 -----> DATA        *
 ***************************/

#include <ioCC2530.h>
#include "OnBoard.h"
#include "relays.h"

#define PIN     P0_5
#define PIN_1   P2_0

void Init_Relays(void)
{
  P0DIR |= 0x20;    //输出
  PIN = 1;
}

void Relays_On(void)
{
  PIN = 0;
}

void Relays_Off(void)
{
  PIN = 1;
}


//由于协调器用到串口功能，不能使用P0_5，所以使用P2_0代替
void Init_Relays_1(void)
{
  P2DIR |= 0x01;    //输出
  PIN_1 = 0;
}

void Relays_On_1(void)
{
  PIN_1 = 1;
}

void Relays_Off_1(void)
{
  PIN_1 = 0;
}