/***************************
 *     雨滴模块(节点1)     *
 * P0.6 -----> AO 模拟引脚 *
 * p1.5 -----> DO 数字引脚 *
 ***************************/

#ifndef __RAIN_H__
#define __RAIN_H__

extern uint8 GetRain(void);        //获取雨水百分比

extern uint8 GetRainH_L(void);     //获取雨滴传感器数字引脚信号
                                    //超出阈值，返回1；否则返回0
#endif