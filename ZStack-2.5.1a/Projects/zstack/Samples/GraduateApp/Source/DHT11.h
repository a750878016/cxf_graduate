/***************************
 *    温湿度模块(节点1)    *
 * P0.7 -----> DATA        *
 ***************************/

#ifndef __DHT11_H__
#define __DHT11_H__

#define uchar unsigned char
#define uint  unsigned int


extern void DHT11(void);                //获取温湿度

extern uchar shidu,wendu;               //湿度，温度

#endif