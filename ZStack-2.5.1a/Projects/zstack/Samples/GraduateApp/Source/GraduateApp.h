#ifndef GRADUATEAPP_H
#define GRADUATEAPP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "ZComDef.h"

/*
 *简单设备描述符宏定义
 */
#define GRADUATEAPP_ENDPOINT            11          //端口号
#define GRADUATEAPP_PROFID              0x0F05      //应用规范ID
#define GRADUATEAPP_DEVICEID            0x0001      //设备ID
#define GRADUATEAPP_DEVICE_VERSION      0           //应用设备版本号
#define GRADUATEAPP_FLAGS               0           //标识
  
#define GRADUATE_MAX_CLUSTERS           2           //最大簇
/*簇*/
#define GRADUATEAPP_DATA_CLUSTERID      1           //数据/状态簇
#define GRADUATEAPP_CONTROL_CLUSTERID   2           //控制簇

  //任务序列号
#define COORDINATORAPP_CAPTURE_LIGHTING 0x0001      //协调器数据采集
#define NODEAPP_1_CAPTURE_RAIN          0x0002      //节点一雨滴采集
#define NODEAPP_1_CAPTURE_DHT11         0x0004      //节点一温湿度采集 
#define NODEAPP_2_CAPTURE_MQ2           0x0008      //节点二气体数据采集
#define COORDINATORAPP_LCD_SHOWN        0x0010      //协调器轮询显示数据  

/*
 * 任务初始化
 */
extern void GraduateApp_Init( byte task_id );

/*
 * 任务事件
 */
extern UINT16 GraduateApp_ProcessEvent( byte task_id, UINT16 events );

#ifdef __cplusplus
}
#endif

#endif /* GRADUATEAPP_H */
