/***************************
 *  继电器(所以节点一样)   *
 * P0.5 -----> DATA        *
 ***************************/

#ifndef __RELAYS_H__
#define __RELAYS_H__

extern void Init_Relays(void);  //初始化引脚
extern void Relays_On(void);    //继电器打开
extern void Relays_Off(void);   //继电器关闭

extern void Init_Relays_1(void);  //初始化引脚
extern void Relays_On_1(void);    //继电器打开
extern void Relays_Off_1(void);   //继电器关闭

#endif