/***************************
 *      舵机(节点一)       *
 * P0.4 -----> DATA        *
 ***************************/

#ifndef __SERVOS_H__
#define __SERVOS_H__

extern void InitServos(void);       //初始化舵机
extern void Servos_Start(uint8 n);  //舵机角度：0/45/90/135/180
extern void Servos_Stop(void); //关闭舵机

#endif