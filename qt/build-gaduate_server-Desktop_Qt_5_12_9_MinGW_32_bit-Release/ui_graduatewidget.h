/********************************************************************************
** Form generated from reading UI file 'graduatewidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRADUATEWIDGET_H
#define UI_GRADUATEWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GraduateWidget
{
public:
    QWidget *title_Widget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *title_horizontalSpacer_1;
    QLabel *title_label;
    QSpacerItem *title_horizontalSpacer_2;
    QWidget *video_widget;
    QLabel *video_show;
    QLabel *video_label;
    QPushButton *video_Button;
    QWidget *time_Widget;
    QHBoxLayout *horizontalLayout_2;
    QLCDNumber *time_h;
    QLCDNumber *time_m;
    QLCDNumber *time_s;
    QWidget *server_Widget;
    QVBoxLayout *verticalLayout;
    QWidget *sockrt_Widget;
    QHBoxLayout *horizontalLayout_3;
    QLabel *ip_label;
    QLineEdit *ip_lineEdit;
    QWidget *tcp_Widget;
    QHBoxLayout *horizontalLayout_4;
    QLabel *tcp_port_label;
    QLineEdit *tcp_port;
    QWidget *udp_Widget;
    QHBoxLayout *horizontalLayout_5;
    QLabel *udp_port_label;
    QLineEdit *udp_port;
    QWidget *server_constrol_Widget;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *star_server;
    QSpacerItem *horizontalSpacer;
    QPushButton *stop_server;
    QWidget *message_widget;
    QLabel *message_label;
    QTextEdit *message_textEdit;
    QPushButton *message_Button;
    QTabWidget *tabWidget;
    QWidget *Page1;
    QWidget *data_Widget_1;
    QVBoxLayout *verticalLayout_2;
    QWidget *rain_Widget;
    QHBoxLayout *horizontalLayout_8;
    QLabel *rain_label;
    QLineEdit *rain_lineEdit;
    QWidget *light_Widget;
    QHBoxLayout *horizontalLayout_9;
    QLabel *light_label;
    QLineEdit *light_lineEdit;
    QWidget *data_Widget_2;
    QVBoxLayout *verticalLayout_3;
    QWidget *shidu_Widget;
    QHBoxLayout *horizontalLayout_10;
    QLabel *shidu_label;
    QLineEdit *shidu_lineEdit;
    QWidget *wendu_Widget;
    QHBoxLayout *horizontalLayout_11;
    QLabel *wendu_label;
    QLineEdit *wendu_lineEdit;
    QWidget *data_Widget_3;
    QVBoxLayout *verticalLayout_4;
    QWidget *mq2_Widget;
    QHBoxLayout *horizontalLayout_12;
    QLabel *mq2_label;
    QLineEdit *mq2_lineEdit;
    QWidget *xxxx_Widget;
    QHBoxLayout *horizontalLayout_13;
    QLabel *xxxx_label;
    QLineEdit *xxxx_lineEdit;
    QWidget *page2;
    QWidget *window_widget;
    QPushButton *window_close;
    QLabel *window_label;
    QLineEdit *window_state;
    QPushButton *window_open;
    QWidget *door_widget;
    QPushButton *door_close;
    QLabel *door_label;
    QLineEdit *door_state;
    QPushButton *door_open;
    QWidget *washing_widget;
    QPushButton *washing_close;
    QLabel *washing_label;
    QLineEdit *washing_state;
    QPushButton *washing_open;
    QWidget *air_widget;
    QPushButton *air_close;
    QLabel *air_label;
    QLineEdit *air_state;
    QPushButton *air_open;
    QWidget *led_widget;
    QPushButton *led_close;
    QLabel *led_label;
    QLineEdit *led_state;
    QPushButton *led_open;
    QWidget *heater_widget;
    QPushButton *heater_close;
    QLabel *heater_label;
    QLineEdit *heater_state;
    QPushButton *heater_open;
    QWidget *buzzer_widget;
    QPushButton *buzzer_close;
    QLabel *buzzer_label;
    QLineEdit *buzzer_state;
    QPushButton *buzzer_open;
    QWidget *STM32_widget;
    QPushButton *STM32_close;
    QLabel *STM32_label;
    QLineEdit *STM32_state;
    QPushButton *STM32_open;
    QWidget *Page3;
    QWidget *send_widget;
    QLabel *send_label;
    QLineEdit *send_data;
    QPushButton *send_pushButton;
    QWidget *send_widget_2;
    QLabel *send_label_2;
    QWidget *Page4;
    QWidget *weekday_widget;
    QLineEdit *weekday_time;
    QPushButton *weekday_pushButton;
    QPushButton *weekday_close;
    QCheckBox *weekday_washing;
    QCheckBox *weekday_air;
    QCheckBox *weekday_heater;
    QCheckBox *weekday_clock;
    QWidget *restday_widget;
    QLineEdit *restday_time;
    QPushButton *restday_pushButton;
    QPushButton *restday_close;
    QCheckBox *restday_washing;
    QCheckBox *restday_air;
    QCheckBox *restday_heater;
    QCheckBox *restday_clock;
    QLCDNumber *weekday_lcdNumber;
    QLCDNumber *weekday_lcdNumber_2;
    QLCDNumber *restday_lcdNumber;
    QLCDNumber *restday_lcdNumber_2;
    QWidget *Page5;
    QWidget *setting_widget;
    QPushButton *setting_pushButton;
    QLabel *washing_label_1;
    QLineEdit *washing_lineEdit_1;
    QLabel *heater_label_1;
    QLineEdit *air_lineEdit_1;
    QLabel *air_label_1;
    QLineEdit *heater_lineEdit_1;
    QLabel *maxkw_label;
    QLineEdit *maxkw_lineEdit;
    QPushButton *close_Butten;
    QPushButton *m_hide_Butten;
    QWidget *day_Widget;
    QHBoxLayout *horizontalLayout_7;
    QLCDNumber *day_y;
    QLCDNumber *day_m;
    QLCDNumber *day_d;
    QLineEdit *week_1;

    void setupUi(QWidget *GraduateWidget)
    {
        if (GraduateWidget->objectName().isEmpty())
            GraduateWidget->setObjectName(QString::fromUtf8("GraduateWidget"));
        GraduateWidget->resize(1800, 950);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(GraduateWidget->sizePolicy().hasHeightForWidth());
        GraduateWidget->setSizePolicy(sizePolicy);
        GraduateWidget->setContextMenuPolicy(Qt::DefaultContextMenu);
        GraduateWidget->setAutoFillBackground(false);
        GraduateWidget->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";"));
        title_Widget = new QWidget(GraduateWidget);
        title_Widget->setObjectName(QString::fromUtf8("title_Widget"));
        title_Widget->setGeometry(QRect(20, 30, 1760, 50));
        sizePolicy.setHeightForWidth(title_Widget->sizePolicy().hasHeightForWidth());
        title_Widget->setSizePolicy(sizePolicy);
        title_Widget->setSizeIncrement(QSize(0, 0));
        title_Widget->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"background-color: rgb(170, 255, 255);"));
        horizontalLayout = new QHBoxLayout(title_Widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        title_horizontalSpacer_1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(title_horizontalSpacer_1);

        title_label = new QLabel(title_Widget);
        title_label->setObjectName(QString::fromUtf8("title_label"));
        title_label->setEnabled(true);
        title_label->setStyleSheet(QString::fromUtf8("color: qconicalgradient(cx:0.5, cy:0.5, angle:0, stop:0 rgba(255, 255, 255, 255), stop:0.373979 rgba(255, 255, 255, 255), stop:0.373991 rgba(33, 30, 255, 255), stop:0.624018 rgba(33, 30, 255, 255), stop:0.624043 rgba(255, 0, 0, 255), stop:1 rgba(255, 0, 0, 255));\n"
"font: 20pt \"Arial\";"));
        title_label->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(title_label);

        title_horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(title_horizontalSpacer_2);

        video_widget = new QWidget(GraduateWidget);
        video_widget->setObjectName(QString::fromUtf8("video_widget"));
        video_widget->setGeometry(QRect(20, 100, 644, 582));
        video_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 255);"));
        video_show = new QLabel(video_widget);
        video_show->setObjectName(QString::fromUtf8("video_show"));
        video_show->setEnabled(true);
        video_show->setGeometry(QRect(2, 100, 640, 480));
        sizePolicy.setHeightForWidth(video_show->sizePolicy().hasHeightForWidth());
        video_show->setSizePolicy(sizePolicy);
        video_show->setMinimumSize(QSize(0, 0));
        video_show->setMaximumSize(QSize(640, 480));
        video_show->setLayoutDirection(Qt::LeftToRight);
        video_show->setAutoFillBackground(false);
        video_show->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        video_show->setFrameShape(QFrame::Box);
        video_show->setFrameShadow(QFrame::Raised);
        video_show->setTextFormat(Qt::AutoText);
        video_label = new QLabel(video_widget);
        video_label->setObjectName(QString::fromUtf8("video_label"));
        video_label->setGeometry(QRect(60, 30, 110, 40));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(video_label->sizePolicy().hasHeightForWidth());
        video_label->setSizePolicy(sizePolicy1);
        video_label->setMaximumSize(QSize(110, 40));
        video_label->setStyleSheet(QString::fromUtf8("font: 16pt \"Arial\";"));
        video_Button = new QPushButton(video_widget);
        video_Button->setObjectName(QString::fromUtf8("video_Button"));
        video_Button->setGeometry(QRect(440, 30, 130, 40));
        video_Button->setStyleSheet(QString::fromUtf8("font: 16pt \"Arial\";\n"
"color: rgb(255, 0, 0);"));
        time_Widget = new QWidget(GraduateWidget);
        time_Widget->setObjectName(QString::fromUtf8("time_Widget"));
        time_Widget->setGeometry(QRect(720, 530, 351, 71));
        time_Widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 255);"));
        horizontalLayout_2 = new QHBoxLayout(time_Widget);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        time_h = new QLCDNumber(time_Widget);
        time_h->setObjectName(QString::fromUtf8("time_h"));
        time_h->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(255, 0, 0);"));

        horizontalLayout_2->addWidget(time_h);

        time_m = new QLCDNumber(time_Widget);
        time_m->setObjectName(QString::fromUtf8("time_m"));
        time_m->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(255, 0, 0);"));

        horizontalLayout_2->addWidget(time_m);

        time_s = new QLCDNumber(time_Widget);
        time_s->setObjectName(QString::fromUtf8("time_s"));
        time_s->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(255, 0, 0);"));

        horizontalLayout_2->addWidget(time_s);

        server_Widget = new QWidget(GraduateWidget);
        server_Widget->setObjectName(QString::fromUtf8("server_Widget"));
        server_Widget->setGeometry(QRect(720, 120, 350, 220));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(server_Widget->sizePolicy().hasHeightForWidth());
        server_Widget->setSizePolicy(sizePolicy2);
        server_Widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 255);"));
        verticalLayout = new QVBoxLayout(server_Widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        sockrt_Widget = new QWidget(server_Widget);
        sockrt_Widget->setObjectName(QString::fromUtf8("sockrt_Widget"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(sockrt_Widget->sizePolicy().hasHeightForWidth());
        sockrt_Widget->setSizePolicy(sizePolicy3);
        horizontalLayout_3 = new QHBoxLayout(sockrt_Widget);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        ip_label = new QLabel(sockrt_Widget);
        ip_label->setObjectName(QString::fromUtf8("ip_label"));
        ip_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"background-color: rgb(255, 255, 255);"));
        ip_label->setFrameShape(QFrame::Box);

        horizontalLayout_3->addWidget(ip_label);

        ip_lineEdit = new QLineEdit(sockrt_Widget);
        ip_lineEdit->setObjectName(QString::fromUtf8("ip_lineEdit"));
        sizePolicy2.setHeightForWidth(ip_lineEdit->sizePolicy().hasHeightForWidth());
        ip_lineEdit->setSizePolicy(sizePolicy2);
        ip_lineEdit->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"background-color: rgb(255, 255, 255);"));
        ip_lineEdit->setAlignment(Qt::AlignCenter);
        ip_lineEdit->setReadOnly(true);

        horizontalLayout_3->addWidget(ip_lineEdit);


        verticalLayout->addWidget(sockrt_Widget);

        tcp_Widget = new QWidget(server_Widget);
        tcp_Widget->setObjectName(QString::fromUtf8("tcp_Widget"));
        sizePolicy3.setHeightForWidth(tcp_Widget->sizePolicy().hasHeightForWidth());
        tcp_Widget->setSizePolicy(sizePolicy3);
        horizontalLayout_4 = new QHBoxLayout(tcp_Widget);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        tcp_port_label = new QLabel(tcp_Widget);
        tcp_port_label->setObjectName(QString::fromUtf8("tcp_port_label"));
        tcp_port_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"background-color: rgb(255, 255, 255);"));
        tcp_port_label->setFrameShape(QFrame::Box);

        horizontalLayout_4->addWidget(tcp_port_label);

        tcp_port = new QLineEdit(tcp_Widget);
        tcp_port->setObjectName(QString::fromUtf8("tcp_port"));
        sizePolicy2.setHeightForWidth(tcp_port->sizePolicy().hasHeightForWidth());
        tcp_port->setSizePolicy(sizePolicy2);
        tcp_port->setLayoutDirection(Qt::LeftToRight);
        tcp_port->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"background-color: rgb(255, 255, 255);"));
        tcp_port->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(tcp_port);


        verticalLayout->addWidget(tcp_Widget);

        udp_Widget = new QWidget(server_Widget);
        udp_Widget->setObjectName(QString::fromUtf8("udp_Widget"));
        sizePolicy3.setHeightForWidth(udp_Widget->sizePolicy().hasHeightForWidth());
        udp_Widget->setSizePolicy(sizePolicy3);
        horizontalLayout_5 = new QHBoxLayout(udp_Widget);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        udp_port_label = new QLabel(udp_Widget);
        udp_port_label->setObjectName(QString::fromUtf8("udp_port_label"));
        udp_port_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"background-color: rgb(255, 255, 255);"));
        udp_port_label->setFrameShape(QFrame::Box);

        horizontalLayout_5->addWidget(udp_port_label);

        udp_port = new QLineEdit(udp_Widget);
        udp_port->setObjectName(QString::fromUtf8("udp_port"));
        sizePolicy2.setHeightForWidth(udp_port->sizePolicy().hasHeightForWidth());
        udp_port->setSizePolicy(sizePolicy2);
        udp_port->setLayoutDirection(Qt::LeftToRight);
        udp_port->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"background-color: rgb(255, 255, 255);"));
        udp_port->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(udp_port);


        verticalLayout->addWidget(udp_Widget);

        server_constrol_Widget = new QWidget(GraduateWidget);
        server_constrol_Widget->setObjectName(QString::fromUtf8("server_constrol_Widget"));
        server_constrol_Widget->setGeometry(QRect(720, 360, 350, 71));
        server_constrol_Widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 255);"));
        horizontalLayout_6 = new QHBoxLayout(server_constrol_Widget);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        star_server = new QPushButton(server_constrol_Widget);
        star_server->setObjectName(QString::fromUtf8("star_server"));
        star_server->setStyleSheet(QString::fromUtf8("font: 16pt \"Arial\";\n"
"color: rgb(255, 0, 0);"));

        horizontalLayout_6->addWidget(star_server);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer);

        stop_server = new QPushButton(server_constrol_Widget);
        stop_server->setObjectName(QString::fromUtf8("stop_server"));
        stop_server->setStyleSheet(QString::fromUtf8("font: 16pt \"Arial\";\n"
"color: rgb(255, 0, 0);"));

        horizontalLayout_6->addWidget(stop_server);

        message_widget = new QWidget(GraduateWidget);
        message_widget->setObjectName(QString::fromUtf8("message_widget"));
        message_widget->setGeometry(QRect(1130, 100, 644, 582));
        message_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 255);"));
        message_label = new QLabel(message_widget);
        message_label->setObjectName(QString::fromUtf8("message_label"));
        message_label->setGeometry(QRect(60, 30, 110, 40));
        sizePolicy1.setHeightForWidth(message_label->sizePolicy().hasHeightForWidth());
        message_label->setSizePolicy(sizePolicy1);
        message_label->setMaximumSize(QSize(110, 40));
        message_label->setStyleSheet(QString::fromUtf8("font: 16pt \"Arial\";"));
        message_textEdit = new QTextEdit(message_widget);
        message_textEdit->setObjectName(QString::fromUtf8("message_textEdit"));
        message_textEdit->setGeometry(QRect(2, 100, 640, 480));
        message_textEdit->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        message_textEdit->setReadOnly(true);
        message_Button = new QPushButton(message_widget);
        message_Button->setObjectName(QString::fromUtf8("message_Button"));
        message_Button->setGeometry(QRect(440, 30, 130, 40));
        message_Button->setStyleSheet(QString::fromUtf8("font: 16pt \"Arial\";\n"
"color: rgb(255, 0, 0);"));
        tabWidget = new QTabWidget(GraduateWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(20, 700, 1760, 220));
        tabWidget->setFocusPolicy(Qt::WheelFocus);
        tabWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
        tabWidget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 255);\n"
"color: rgb(255, 0, 0);"));
        tabWidget->setTabPosition(QTabWidget::South);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setTabBarAutoHide(false);
        Page1 = new QWidget();
        Page1->setObjectName(QString::fromUtf8("Page1"));
        data_Widget_1 = new QWidget(Page1);
        data_Widget_1->setObjectName(QString::fromUtf8("data_Widget_1"));
        data_Widget_1->setGeometry(QRect(20, 15, 160, 160));
        data_Widget_1->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        verticalLayout_2 = new QVBoxLayout(data_Widget_1);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        rain_Widget = new QWidget(data_Widget_1);
        rain_Widget->setObjectName(QString::fromUtf8("rain_Widget"));
        horizontalLayout_8 = new QHBoxLayout(rain_Widget);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        rain_label = new QLabel(rain_Widget);
        rain_label->setObjectName(QString::fromUtf8("rain_label"));
        rain_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);"));

        horizontalLayout_8->addWidget(rain_label);

        rain_lineEdit = new QLineEdit(rain_Widget);
        rain_lineEdit->setObjectName(QString::fromUtf8("rain_lineEdit"));
        rain_lineEdit->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        rain_lineEdit->setAlignment(Qt::AlignCenter);
        rain_lineEdit->setReadOnly(true);

        horizontalLayout_8->addWidget(rain_lineEdit);


        verticalLayout_2->addWidget(rain_Widget);

        light_Widget = new QWidget(data_Widget_1);
        light_Widget->setObjectName(QString::fromUtf8("light_Widget"));
        horizontalLayout_9 = new QHBoxLayout(light_Widget);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        light_label = new QLabel(light_Widget);
        light_label->setObjectName(QString::fromUtf8("light_label"));
        light_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);"));

        horizontalLayout_9->addWidget(light_label);

        light_lineEdit = new QLineEdit(light_Widget);
        light_lineEdit->setObjectName(QString::fromUtf8("light_lineEdit"));
        light_lineEdit->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        light_lineEdit->setAlignment(Qt::AlignCenter);
        light_lineEdit->setReadOnly(true);

        horizontalLayout_9->addWidget(light_lineEdit);


        verticalLayout_2->addWidget(light_Widget);

        data_Widget_2 = new QWidget(Page1);
        data_Widget_2->setObjectName(QString::fromUtf8("data_Widget_2"));
        data_Widget_2->setGeometry(QRect(205, 15, 160, 160));
        data_Widget_2->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        verticalLayout_3 = new QVBoxLayout(data_Widget_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        shidu_Widget = new QWidget(data_Widget_2);
        shidu_Widget->setObjectName(QString::fromUtf8("shidu_Widget"));
        horizontalLayout_10 = new QHBoxLayout(shidu_Widget);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        shidu_label = new QLabel(shidu_Widget);
        shidu_label->setObjectName(QString::fromUtf8("shidu_label"));
        shidu_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);"));

        horizontalLayout_10->addWidget(shidu_label);

        shidu_lineEdit = new QLineEdit(shidu_Widget);
        shidu_lineEdit->setObjectName(QString::fromUtf8("shidu_lineEdit"));
        shidu_lineEdit->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        shidu_lineEdit->setAlignment(Qt::AlignCenter);
        shidu_lineEdit->setReadOnly(true);

        horizontalLayout_10->addWidget(shidu_lineEdit);


        verticalLayout_3->addWidget(shidu_Widget);

        wendu_Widget = new QWidget(data_Widget_2);
        wendu_Widget->setObjectName(QString::fromUtf8("wendu_Widget"));
        horizontalLayout_11 = new QHBoxLayout(wendu_Widget);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        wendu_label = new QLabel(wendu_Widget);
        wendu_label->setObjectName(QString::fromUtf8("wendu_label"));
        wendu_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);"));

        horizontalLayout_11->addWidget(wendu_label);

        wendu_lineEdit = new QLineEdit(wendu_Widget);
        wendu_lineEdit->setObjectName(QString::fromUtf8("wendu_lineEdit"));
        wendu_lineEdit->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        wendu_lineEdit->setAlignment(Qt::AlignCenter);
        wendu_lineEdit->setReadOnly(true);

        horizontalLayout_11->addWidget(wendu_lineEdit);


        verticalLayout_3->addWidget(wendu_Widget);

        data_Widget_3 = new QWidget(Page1);
        data_Widget_3->setObjectName(QString::fromUtf8("data_Widget_3"));
        data_Widget_3->setGeometry(QRect(390, 15, 241, 160));
        data_Widget_3->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        verticalLayout_4 = new QVBoxLayout(data_Widget_3);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        mq2_Widget = new QWidget(data_Widget_3);
        mq2_Widget->setObjectName(QString::fromUtf8("mq2_Widget"));
        mq2_Widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        horizontalLayout_12 = new QHBoxLayout(mq2_Widget);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        mq2_label = new QLabel(mq2_Widget);
        mq2_label->setObjectName(QString::fromUtf8("mq2_label"));
        sizePolicy3.setHeightForWidth(mq2_label->sizePolicy().hasHeightForWidth());
        mq2_label->setSizePolicy(sizePolicy3);
        mq2_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);"));

        horizontalLayout_12->addWidget(mq2_label);

        mq2_lineEdit = new QLineEdit(mq2_Widget);
        mq2_lineEdit->setObjectName(QString::fromUtf8("mq2_lineEdit"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(mq2_lineEdit->sizePolicy().hasHeightForWidth());
        mq2_lineEdit->setSizePolicy(sizePolicy4);
        mq2_lineEdit->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        mq2_lineEdit->setAlignment(Qt::AlignCenter);
        mq2_lineEdit->setReadOnly(true);

        horizontalLayout_12->addWidget(mq2_lineEdit);


        verticalLayout_4->addWidget(mq2_Widget);

        xxxx_Widget = new QWidget(data_Widget_3);
        xxxx_Widget->setObjectName(QString::fromUtf8("xxxx_Widget"));
        horizontalLayout_13 = new QHBoxLayout(xxxx_Widget);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        xxxx_label = new QLabel(xxxx_Widget);
        xxxx_label->setObjectName(QString::fromUtf8("xxxx_label"));
        xxxx_label->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);"));

        horizontalLayout_13->addWidget(xxxx_label);

        xxxx_lineEdit = new QLineEdit(xxxx_Widget);
        xxxx_lineEdit->setObjectName(QString::fromUtf8("xxxx_lineEdit"));
        xxxx_lineEdit->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        xxxx_lineEdit->setAlignment(Qt::AlignCenter);
        xxxx_lineEdit->setReadOnly(true);

        horizontalLayout_13->addWidget(xxxx_lineEdit);


        verticalLayout_4->addWidget(xxxx_Widget);

        tabWidget->addTab(Page1, QString());
        page2 = new QWidget();
        page2->setObjectName(QString::fromUtf8("page2"));
        window_widget = new QWidget(page2);
        window_widget->setObjectName(QString::fromUtf8("window_widget"));
        window_widget->setGeometry(QRect(20, 20, 381, 61));
        window_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        window_close = new QPushButton(window_widget);
        window_close->setObjectName(QString::fromUtf8("window_close"));
        window_close->setGeometry(QRect(290, 10, 80, 45));
        sizePolicy2.setHeightForWidth(window_close->sizePolicy().hasHeightForWidth());
        window_close->setSizePolicy(sizePolicy2);
        window_close->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        window_label = new QLabel(window_widget);
        window_label->setObjectName(QString::fromUtf8("window_label"));
        window_label->setGeometry(QRect(10, 10, 80, 45));
        sizePolicy2.setHeightForWidth(window_label->sizePolicy().hasHeightForWidth());
        window_label->setSizePolicy(sizePolicy2);
        window_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(170, 255, 127);"));
        window_label->setFrameShape(QFrame::NoFrame);
        window_label->setAlignment(Qt::AlignCenter);
        window_state = new QLineEdit(window_widget);
        window_state->setObjectName(QString::fromUtf8("window_state"));
        window_state->setGeometry(QRect(110, 10, 80, 45));
        sizePolicy2.setHeightForWidth(window_state->sizePolicy().hasHeightForWidth());
        window_state->setSizePolicy(sizePolicy2);
        window_state->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        window_state->setAlignment(Qt::AlignCenter);
        window_state->setReadOnly(true);
        window_open = new QPushButton(window_widget);
        window_open->setObjectName(QString::fromUtf8("window_open"));
        window_open->setGeometry(QRect(200, 10, 80, 45));
        sizePolicy2.setHeightForWidth(window_open->sizePolicy().hasHeightForWidth());
        window_open->setSizePolicy(sizePolicy2);
        window_open->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        door_widget = new QWidget(page2);
        door_widget->setObjectName(QString::fromUtf8("door_widget"));
        door_widget->setGeometry(QRect(20, 100, 381, 61));
        door_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        door_close = new QPushButton(door_widget);
        door_close->setObjectName(QString::fromUtf8("door_close"));
        door_close->setGeometry(QRect(290, 10, 80, 45));
        sizePolicy2.setHeightForWidth(door_close->sizePolicy().hasHeightForWidth());
        door_close->setSizePolicy(sizePolicy2);
        door_close->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        door_label = new QLabel(door_widget);
        door_label->setObjectName(QString::fromUtf8("door_label"));
        door_label->setGeometry(QRect(10, 10, 80, 45));
        sizePolicy2.setHeightForWidth(door_label->sizePolicy().hasHeightForWidth());
        door_label->setSizePolicy(sizePolicy2);
        door_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(170, 255, 127);"));
        door_label->setFrameShape(QFrame::NoFrame);
        door_label->setAlignment(Qt::AlignCenter);
        door_state = new QLineEdit(door_widget);
        door_state->setObjectName(QString::fromUtf8("door_state"));
        door_state->setGeometry(QRect(110, 10, 80, 45));
        sizePolicy2.setHeightForWidth(door_state->sizePolicy().hasHeightForWidth());
        door_state->setSizePolicy(sizePolicy2);
        door_state->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        door_state->setAlignment(Qt::AlignCenter);
        door_state->setReadOnly(true);
        door_open = new QPushButton(door_widget);
        door_open->setObjectName(QString::fromUtf8("door_open"));
        door_open->setGeometry(QRect(200, 10, 80, 45));
        sizePolicy2.setHeightForWidth(door_open->sizePolicy().hasHeightForWidth());
        door_open->setSizePolicy(sizePolicy2);
        door_open->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        washing_widget = new QWidget(page2);
        washing_widget->setObjectName(QString::fromUtf8("washing_widget"));
        washing_widget->setGeometry(QRect(430, 20, 381, 61));
        washing_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        washing_close = new QPushButton(washing_widget);
        washing_close->setObjectName(QString::fromUtf8("washing_close"));
        washing_close->setGeometry(QRect(290, 10, 80, 45));
        sizePolicy2.setHeightForWidth(washing_close->sizePolicy().hasHeightForWidth());
        washing_close->setSizePolicy(sizePolicy2);
        washing_close->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        washing_label = new QLabel(washing_widget);
        washing_label->setObjectName(QString::fromUtf8("washing_label"));
        washing_label->setGeometry(QRect(10, 10, 80, 45));
        sizePolicy2.setHeightForWidth(washing_label->sizePolicy().hasHeightForWidth());
        washing_label->setSizePolicy(sizePolicy2);
        washing_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(170, 255, 127);"));
        washing_label->setFrameShape(QFrame::NoFrame);
        washing_label->setAlignment(Qt::AlignCenter);
        washing_state = new QLineEdit(washing_widget);
        washing_state->setObjectName(QString::fromUtf8("washing_state"));
        washing_state->setGeometry(QRect(110, 10, 80, 45));
        sizePolicy2.setHeightForWidth(washing_state->sizePolicy().hasHeightForWidth());
        washing_state->setSizePolicy(sizePolicy2);
        washing_state->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        washing_state->setAlignment(Qt::AlignCenter);
        washing_state->setReadOnly(true);
        washing_open = new QPushButton(washing_widget);
        washing_open->setObjectName(QString::fromUtf8("washing_open"));
        washing_open->setGeometry(QRect(200, 10, 80, 45));
        sizePolicy2.setHeightForWidth(washing_open->sizePolicy().hasHeightForWidth());
        washing_open->setSizePolicy(sizePolicy2);
        washing_open->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        air_widget = new QWidget(page2);
        air_widget->setObjectName(QString::fromUtf8("air_widget"));
        air_widget->setGeometry(QRect(430, 100, 381, 61));
        air_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        air_close = new QPushButton(air_widget);
        air_close->setObjectName(QString::fromUtf8("air_close"));
        air_close->setGeometry(QRect(290, 10, 80, 45));
        sizePolicy2.setHeightForWidth(air_close->sizePolicy().hasHeightForWidth());
        air_close->setSizePolicy(sizePolicy2);
        air_close->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        air_label = new QLabel(air_widget);
        air_label->setObjectName(QString::fromUtf8("air_label"));
        air_label->setGeometry(QRect(10, 10, 80, 45));
        sizePolicy2.setHeightForWidth(air_label->sizePolicy().hasHeightForWidth());
        air_label->setSizePolicy(sizePolicy2);
        air_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(170, 255, 127);"));
        air_label->setFrameShape(QFrame::NoFrame);
        air_label->setAlignment(Qt::AlignCenter);
        air_state = new QLineEdit(air_widget);
        air_state->setObjectName(QString::fromUtf8("air_state"));
        air_state->setGeometry(QRect(110, 10, 80, 45));
        sizePolicy2.setHeightForWidth(air_state->sizePolicy().hasHeightForWidth());
        air_state->setSizePolicy(sizePolicy2);
        air_state->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        air_state->setAlignment(Qt::AlignCenter);
        air_state->setReadOnly(true);
        air_open = new QPushButton(air_widget);
        air_open->setObjectName(QString::fromUtf8("air_open"));
        air_open->setGeometry(QRect(200, 10, 80, 45));
        sizePolicy2.setHeightForWidth(air_open->sizePolicy().hasHeightForWidth());
        air_open->setSizePolicy(sizePolicy2);
        air_open->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        led_widget = new QWidget(page2);
        led_widget->setObjectName(QString::fromUtf8("led_widget"));
        led_widget->setGeometry(QRect(840, 100, 381, 61));
        led_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        led_close = new QPushButton(led_widget);
        led_close->setObjectName(QString::fromUtf8("led_close"));
        led_close->setGeometry(QRect(290, 10, 80, 45));
        sizePolicy2.setHeightForWidth(led_close->sizePolicy().hasHeightForWidth());
        led_close->setSizePolicy(sizePolicy2);
        led_close->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        led_label = new QLabel(led_widget);
        led_label->setObjectName(QString::fromUtf8("led_label"));
        led_label->setGeometry(QRect(10, 10, 80, 45));
        sizePolicy2.setHeightForWidth(led_label->sizePolicy().hasHeightForWidth());
        led_label->setSizePolicy(sizePolicy2);
        led_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(170, 255, 127);"));
        led_label->setFrameShape(QFrame::NoFrame);
        led_label->setAlignment(Qt::AlignCenter);
        led_state = new QLineEdit(led_widget);
        led_state->setObjectName(QString::fromUtf8("led_state"));
        led_state->setGeometry(QRect(110, 10, 80, 45));
        sizePolicy2.setHeightForWidth(led_state->sizePolicy().hasHeightForWidth());
        led_state->setSizePolicy(sizePolicy2);
        led_state->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        led_state->setAlignment(Qt::AlignCenter);
        led_state->setReadOnly(true);
        led_open = new QPushButton(led_widget);
        led_open->setObjectName(QString::fromUtf8("led_open"));
        led_open->setGeometry(QRect(200, 10, 80, 45));
        sizePolicy2.setHeightForWidth(led_open->sizePolicy().hasHeightForWidth());
        led_open->setSizePolicy(sizePolicy2);
        led_open->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        heater_widget = new QWidget(page2);
        heater_widget->setObjectName(QString::fromUtf8("heater_widget"));
        heater_widget->setGeometry(QRect(840, 20, 381, 61));
        heater_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        heater_close = new QPushButton(heater_widget);
        heater_close->setObjectName(QString::fromUtf8("heater_close"));
        heater_close->setGeometry(QRect(290, 10, 80, 45));
        sizePolicy2.setHeightForWidth(heater_close->sizePolicy().hasHeightForWidth());
        heater_close->setSizePolicy(sizePolicy2);
        heater_close->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        heater_label = new QLabel(heater_widget);
        heater_label->setObjectName(QString::fromUtf8("heater_label"));
        heater_label->setGeometry(QRect(10, 10, 80, 45));
        sizePolicy2.setHeightForWidth(heater_label->sizePolicy().hasHeightForWidth());
        heater_label->setSizePolicy(sizePolicy2);
        heater_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(170, 255, 127);"));
        heater_label->setFrameShape(QFrame::NoFrame);
        heater_label->setAlignment(Qt::AlignCenter);
        heater_state = new QLineEdit(heater_widget);
        heater_state->setObjectName(QString::fromUtf8("heater_state"));
        heater_state->setGeometry(QRect(110, 10, 80, 45));
        sizePolicy2.setHeightForWidth(heater_state->sizePolicy().hasHeightForWidth());
        heater_state->setSizePolicy(sizePolicy2);
        heater_state->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        heater_state->setAlignment(Qt::AlignCenter);
        heater_state->setReadOnly(true);
        heater_open = new QPushButton(heater_widget);
        heater_open->setObjectName(QString::fromUtf8("heater_open"));
        heater_open->setGeometry(QRect(200, 10, 80, 45));
        sizePolicy2.setHeightForWidth(heater_open->sizePolicy().hasHeightForWidth());
        heater_open->setSizePolicy(sizePolicy2);
        heater_open->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        buzzer_widget = new QWidget(page2);
        buzzer_widget->setObjectName(QString::fromUtf8("buzzer_widget"));
        buzzer_widget->setGeometry(QRect(1250, 20, 381, 61));
        buzzer_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        buzzer_close = new QPushButton(buzzer_widget);
        buzzer_close->setObjectName(QString::fromUtf8("buzzer_close"));
        buzzer_close->setGeometry(QRect(290, 10, 80, 45));
        sizePolicy2.setHeightForWidth(buzzer_close->sizePolicy().hasHeightForWidth());
        buzzer_close->setSizePolicy(sizePolicy2);
        buzzer_close->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        buzzer_label = new QLabel(buzzer_widget);
        buzzer_label->setObjectName(QString::fromUtf8("buzzer_label"));
        buzzer_label->setGeometry(QRect(10, 10, 80, 45));
        sizePolicy2.setHeightForWidth(buzzer_label->sizePolicy().hasHeightForWidth());
        buzzer_label->setSizePolicy(sizePolicy2);
        buzzer_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(170, 255, 127);"));
        buzzer_label->setFrameShape(QFrame::NoFrame);
        buzzer_label->setAlignment(Qt::AlignCenter);
        buzzer_state = new QLineEdit(buzzer_widget);
        buzzer_state->setObjectName(QString::fromUtf8("buzzer_state"));
        buzzer_state->setGeometry(QRect(110, 10, 80, 45));
        sizePolicy2.setHeightForWidth(buzzer_state->sizePolicy().hasHeightForWidth());
        buzzer_state->setSizePolicy(sizePolicy2);
        buzzer_state->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        buzzer_state->setAlignment(Qt::AlignCenter);
        buzzer_state->setReadOnly(true);
        buzzer_open = new QPushButton(buzzer_widget);
        buzzer_open->setObjectName(QString::fromUtf8("buzzer_open"));
        buzzer_open->setGeometry(QRect(200, 10, 80, 45));
        sizePolicy2.setHeightForWidth(buzzer_open->sizePolicy().hasHeightForWidth());
        buzzer_open->setSizePolicy(sizePolicy2);
        buzzer_open->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        STM32_widget = new QWidget(page2);
        STM32_widget->setObjectName(QString::fromUtf8("STM32_widget"));
        STM32_widget->setGeometry(QRect(1250, 100, 381, 61));
        STM32_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        STM32_close = new QPushButton(STM32_widget);
        STM32_close->setObjectName(QString::fromUtf8("STM32_close"));
        STM32_close->setGeometry(QRect(290, 10, 80, 45));
        sizePolicy2.setHeightForWidth(STM32_close->sizePolicy().hasHeightForWidth());
        STM32_close->setSizePolicy(sizePolicy2);
        STM32_close->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        STM32_label = new QLabel(STM32_widget);
        STM32_label->setObjectName(QString::fromUtf8("STM32_label"));
        STM32_label->setGeometry(QRect(10, 10, 80, 45));
        sizePolicy2.setHeightForWidth(STM32_label->sizePolicy().hasHeightForWidth());
        STM32_label->setSizePolicy(sizePolicy2);
        STM32_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(170, 255, 127);"));
        STM32_label->setFrameShape(QFrame::NoFrame);
        STM32_label->setAlignment(Qt::AlignCenter);
        STM32_state = new QLineEdit(STM32_widget);
        STM32_state->setObjectName(QString::fromUtf8("STM32_state"));
        STM32_state->setGeometry(QRect(110, 10, 80, 45));
        sizePolicy2.setHeightForWidth(STM32_state->sizePolicy().hasHeightForWidth());
        STM32_state->setSizePolicy(sizePolicy2);
        STM32_state->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);"));
        STM32_state->setAlignment(Qt::AlignCenter);
        STM32_state->setReadOnly(true);
        STM32_open = new QPushButton(STM32_widget);
        STM32_open->setObjectName(QString::fromUtf8("STM32_open"));
        STM32_open->setGeometry(QRect(200, 10, 80, 45));
        sizePolicy2.setHeightForWidth(STM32_open->sizePolicy().hasHeightForWidth());
        STM32_open->setSizePolicy(sizePolicy2);
        STM32_open->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(255, 0, 0);"));
        tabWidget->addTab(page2, QString());
        Page3 = new QWidget();
        Page3->setObjectName(QString::fromUtf8("Page3"));
        send_widget = new QWidget(Page3);
        send_widget->setObjectName(QString::fromUtf8("send_widget"));
        send_widget->setGeometry(QRect(510, 90, 720, 70));
        send_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        send_label = new QLabel(send_widget);
        send_label->setObjectName(QString::fromUtf8("send_label"));
        send_label->setGeometry(QRect(10, 10, 70, 50));
        send_label->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        send_label->setAlignment(Qt::AlignCenter);
        send_data = new QLineEdit(send_widget);
        send_data->setObjectName(QString::fromUtf8("send_data"));
        send_data->setGeometry(QRect(90, 10, 540, 50));
        send_data->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);"));
        send_pushButton = new QPushButton(send_widget);
        send_pushButton->setObjectName(QString::fromUtf8("send_pushButton"));
        send_pushButton->setGeometry(QRect(640, 10, 70, 50));
        send_pushButton->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"background-color: rgb(170, 255, 127);"));
        send_widget_2 = new QWidget(Page3);
        send_widget_2->setObjectName(QString::fromUtf8("send_widget_2"));
        send_widget_2->setGeometry(QRect(510, 10, 720, 70));
        send_widget_2->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        send_label_2 = new QLabel(send_widget_2);
        send_label_2->setObjectName(QString::fromUtf8("send_label_2"));
        send_label_2->setGeometry(QRect(15, 10, 690, 50));
        send_label_2->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);\n"
"color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        send_label_2->setFrameShape(QFrame::Box);
        send_label_2->setAlignment(Qt::AlignCenter);
        tabWidget->addTab(Page3, QString());
        Page4 = new QWidget();
        Page4->setObjectName(QString::fromUtf8("Page4"));
        weekday_widget = new QWidget(Page4);
        weekday_widget->setObjectName(QString::fromUtf8("weekday_widget"));
        weekday_widget->setGeometry(QRect(550, 20, 940, 60));
        weekday_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        weekday_time = new QLineEdit(weekday_widget);
        weekday_time->setObjectName(QString::fromUtf8("weekday_time"));
        weekday_time->setGeometry(QRect(10, 10, 120, 40));
        weekday_time->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);"));
        weekday_pushButton = new QPushButton(weekday_widget);
        weekday_pushButton->setObjectName(QString::fromUtf8("weekday_pushButton"));
        weekday_pushButton->setGeometry(QRect(640, 10, 130, 40));
        weekday_close = new QPushButton(weekday_widget);
        weekday_close->setObjectName(QString::fromUtf8("weekday_close"));
        weekday_close->setGeometry(QRect(790, 10, 130, 40));
        weekday_washing = new QCheckBox(weekday_widget);
        weekday_washing->setObjectName(QString::fromUtf8("weekday_washing"));
        weekday_washing->setGeometry(QRect(150, 20, 90, 20));
        weekday_washing->setStyleSheet(QString::fromUtf8(""));
        weekday_air = new QCheckBox(weekday_widget);
        weekday_air->setObjectName(QString::fromUtf8("weekday_air"));
        weekday_air->setGeometry(QRect(270, 20, 90, 20));
        weekday_heater = new QCheckBox(weekday_widget);
        weekday_heater->setObjectName(QString::fromUtf8("weekday_heater"));
        weekday_heater->setGeometry(QRect(390, 20, 90, 20));
        weekday_clock = new QCheckBox(weekday_widget);
        weekday_clock->setObjectName(QString::fromUtf8("weekday_clock"));
        weekday_clock->setGeometry(QRect(510, 20, 90, 20));
        restday_widget = new QWidget(Page4);
        restday_widget->setObjectName(QString::fromUtf8("restday_widget"));
        restday_widget->setGeometry(QRect(550, 100, 940, 60));
        restday_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        restday_time = new QLineEdit(restday_widget);
        restday_time->setObjectName(QString::fromUtf8("restday_time"));
        restday_time->setGeometry(QRect(10, 10, 120, 40));
        restday_time->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);"));
        restday_pushButton = new QPushButton(restday_widget);
        restday_pushButton->setObjectName(QString::fromUtf8("restday_pushButton"));
        restday_pushButton->setGeometry(QRect(640, 10, 130, 40));
        restday_close = new QPushButton(restday_widget);
        restday_close->setObjectName(QString::fromUtf8("restday_close"));
        restday_close->setGeometry(QRect(790, 10, 130, 40));
        restday_washing = new QCheckBox(restday_widget);
        restday_washing->setObjectName(QString::fromUtf8("restday_washing"));
        restday_washing->setGeometry(QRect(150, 20, 90, 20));
        restday_washing->setStyleSheet(QString::fromUtf8(""));
        restday_air = new QCheckBox(restday_widget);
        restday_air->setObjectName(QString::fromUtf8("restday_air"));
        restday_air->setGeometry(QRect(270, 20, 90, 20));
        restday_heater = new QCheckBox(restday_widget);
        restday_heater->setObjectName(QString::fromUtf8("restday_heater"));
        restday_heater->setGeometry(QRect(390, 20, 90, 20));
        restday_clock = new QCheckBox(restday_widget);
        restday_clock->setObjectName(QString::fromUtf8("restday_clock"));
        restday_clock->setGeometry(QRect(510, 20, 90, 20));
        weekday_lcdNumber = new QLCDNumber(Page4);
        weekday_lcdNumber->setObjectName(QString::fromUtf8("weekday_lcdNumber"));
        weekday_lcdNumber->setGeometry(QRect(370, 20, 160, 60));
        weekday_lcdNumber_2 = new QLCDNumber(Page4);
        weekday_lcdNumber_2->setObjectName(QString::fromUtf8("weekday_lcdNumber_2"));
        weekday_lcdNumber_2->setGeometry(QRect(210, 20, 160, 60));
        restday_lcdNumber = new QLCDNumber(Page4);
        restday_lcdNumber->setObjectName(QString::fromUtf8("restday_lcdNumber"));
        restday_lcdNumber->setGeometry(QRect(370, 100, 160, 60));
        restday_lcdNumber_2 = new QLCDNumber(Page4);
        restday_lcdNumber_2->setObjectName(QString::fromUtf8("restday_lcdNumber_2"));
        restday_lcdNumber_2->setGeometry(QRect(210, 100, 160, 60));
        tabWidget->addTab(Page4, QString());
        Page5 = new QWidget();
        Page5->setObjectName(QString::fromUtf8("Page5"));
        setting_widget = new QWidget(Page5);
        setting_widget->setObjectName(QString::fromUtf8("setting_widget"));
        setting_widget->setGeometry(QRect(280, 60, 1200, 60));
        setting_widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 127);"));
        setting_pushButton = new QPushButton(setting_widget);
        setting_pushButton->setObjectName(QString::fromUtf8("setting_pushButton"));
        setting_pushButton->setGeometry(QRect(1040, 10, 130, 40));
        washing_label_1 = new QLabel(setting_widget);
        washing_label_1->setObjectName(QString::fromUtf8("washing_label_1"));
        washing_label_1->setGeometry(QRect(30, 10, 70, 40));
        washing_label_1->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        washing_label_1->setAlignment(Qt::AlignCenter);
        washing_lineEdit_1 = new QLineEdit(setting_widget);
        washing_lineEdit_1->setObjectName(QString::fromUtf8("washing_lineEdit_1"));
        washing_lineEdit_1->setGeometry(QRect(120, 10, 110, 40));
        washing_lineEdit_1->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        washing_lineEdit_1->setAlignment(Qt::AlignCenter);
        heater_label_1 = new QLabel(setting_widget);
        heater_label_1->setObjectName(QString::fromUtf8("heater_label_1"));
        heater_label_1->setGeometry(QRect(510, 10, 70, 40));
        heater_label_1->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        heater_label_1->setAlignment(Qt::AlignCenter);
        air_lineEdit_1 = new QLineEdit(setting_widget);
        air_lineEdit_1->setObjectName(QString::fromUtf8("air_lineEdit_1"));
        air_lineEdit_1->setGeometry(QRect(350, 10, 110, 40));
        air_lineEdit_1->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        air_lineEdit_1->setAlignment(Qt::AlignCenter);
        air_label_1 = new QLabel(setting_widget);
        air_label_1->setObjectName(QString::fromUtf8("air_label_1"));
        air_label_1->setGeometry(QRect(270, 10, 70, 40));
        air_label_1->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        air_label_1->setAlignment(Qt::AlignCenter);
        heater_lineEdit_1 = new QLineEdit(setting_widget);
        heater_lineEdit_1->setObjectName(QString::fromUtf8("heater_lineEdit_1"));
        heater_lineEdit_1->setGeometry(QRect(600, 10, 110, 40));
        heater_lineEdit_1->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        heater_lineEdit_1->setAlignment(Qt::AlignCenter);
        maxkw_label = new QLabel(setting_widget);
        maxkw_label->setObjectName(QString::fromUtf8("maxkw_label"));
        maxkw_label->setGeometry(QRect(760, 10, 91, 40));
        maxkw_label->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        maxkw_label->setAlignment(Qt::AlignCenter);
        maxkw_lineEdit = new QLineEdit(setting_widget);
        maxkw_lineEdit->setObjectName(QString::fromUtf8("maxkw_lineEdit"));
        maxkw_lineEdit->setGeometry(QRect(880, 10, 110, 40));
        maxkw_lineEdit->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);\n"
"font: 14pt \"Arial\";"));
        maxkw_lineEdit->setAlignment(Qt::AlignCenter);
        tabWidget->addTab(Page5, QString());
        close_Butten = new QPushButton(GraduateWidget);
        close_Butten->setObjectName(QString::fromUtf8("close_Butten"));
        close_Butten->setGeometry(QRect(1670, 890, 101, 41));
        close_Butten->setStyleSheet(QString::fromUtf8("color: rgb(255, 0, 0);"));
        m_hide_Butten = new QPushButton(GraduateWidget);
        m_hide_Butten->setObjectName(QString::fromUtf8("m_hide_Butten"));
        m_hide_Butten->setGeometry(QRect(1550, 890, 101, 41));
        m_hide_Butten->setStyleSheet(QString::fromUtf8("color: rgb(255, 0, 0);"));
        day_Widget = new QWidget(GraduateWidget);
        day_Widget->setObjectName(QString::fromUtf8("day_Widget"));
        day_Widget->setGeometry(QRect(720, 460, 351, 71));
        day_Widget->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 255);\n"
"font: 14pt \"Arial\";"));
        horizontalLayout_7 = new QHBoxLayout(day_Widget);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        day_y = new QLCDNumber(day_Widget);
        day_y->setObjectName(QString::fromUtf8("day_y"));
        day_y->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"font: 14pt \"Arial\";\n"
"color: rgb(255, 0, 0);"));

        horizontalLayout_7->addWidget(day_y);

        day_m = new QLCDNumber(day_Widget);
        day_m->setObjectName(QString::fromUtf8("day_m"));
        day_m->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"font: 14pt \"Arial\";\n"
"color: rgb(255, 0, 0);"));

        horizontalLayout_7->addWidget(day_m);

        day_d = new QLCDNumber(day_Widget);
        day_d->setObjectName(QString::fromUtf8("day_d"));
        day_d->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"font: 14pt \"Arial\";\n"
"color: rgb(255, 0, 0);"));

        horizontalLayout_7->addWidget(day_d);

        week_1 = new QLineEdit(GraduateWidget);
        week_1->setObjectName(QString::fromUtf8("week_1"));
        week_1->setGeometry(QRect(830, 620, 131, 41));
        sizePolicy2.setHeightForWidth(week_1->sizePolicy().hasHeightForWidth());
        week_1->setSizePolicy(sizePolicy2);
        week_1->setLayoutDirection(Qt::LeftToRight);
        week_1->setStyleSheet(QString::fromUtf8("font: 14pt \"Arial\";\n"
"background-color: rgb(255, 255, 255);"));
        week_1->setAlignment(Qt::AlignCenter);
        week_1->setReadOnly(true);

        retranslateUi(GraduateWidget);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(GraduateWidget);
    } // setupUi

    void retranslateUi(QWidget *GraduateWidget)
    {
        GraduateWidget->setWindowTitle(QApplication::translate("GraduateWidget", "IOT", nullptr));
        title_label->setText(QApplication::translate("GraduateWidget", "\346\231\272\350\203\275\345\256\266\345\261\205\347\263\273\347\273\237", nullptr));
        video_show->setText(QString());
        video_label->setText(QApplication::translate("GraduateWidget", "\350\247\206\351\242\221\347\233\221\346\216\247", nullptr));
        video_Button->setText(QApplication::translate("GraduateWidget", "\351\207\207\351\233\206\345\233\276\347\211\207", nullptr));
        ip_label->setText(QApplication::translate("GraduateWidget", "IP\345\234\260\345\235\200\357\274\232", nullptr));
        ip_lineEdit->setText(QString());
        tcp_port_label->setText(QApplication::translate("GraduateWidget", "TCP\347\253\257\345\217\243\345\217\267\357\274\232", nullptr));
        tcp_port->setText(QString());
        udp_port_label->setText(QApplication::translate("GraduateWidget", "UDP\347\253\257\345\217\243\345\217\267\357\274\232", nullptr));
        udp_port->setText(QString());
        star_server->setText(QApplication::translate("GraduateWidget", "\345\274\200\345\220\257", nullptr));
        stop_server->setText(QApplication::translate("GraduateWidget", "\345\205\263\351\227\255", nullptr));
        message_label->setText(QApplication::translate("GraduateWidget", "\344\277\241\346\201\257\346\265\217\350\247\210", nullptr));
        message_Button->setText(QApplication::translate("GraduateWidget", "\346\270\205\347\251\272\351\241\265\351\235\242", nullptr));
        rain_label->setText(QApplication::translate("GraduateWidget", "\351\233\250\346\260\264", nullptr));
        rain_lineEdit->setText(QApplication::translate("GraduateWidget", "89", nullptr));
        light_label->setText(QApplication::translate("GraduateWidget", "\345\205\211\345\274\272", nullptr));
        light_lineEdit->setText(QApplication::translate("GraduateWidget", "67", nullptr));
        shidu_label->setText(QApplication::translate("GraduateWidget", "\346\271\277\345\272\246", nullptr));
        shidu_lineEdit->setText(QApplication::translate("GraduateWidget", "30", nullptr));
        wendu_label->setText(QApplication::translate("GraduateWidget", "\346\270\251\345\272\246", nullptr));
        wendu_lineEdit->setText(QApplication::translate("GraduateWidget", "80", nullptr));
        mq2_label->setText(QApplication::translate("GraduateWidget", "\345\217\257\347\207\203\346\260\224\344\275\223", nullptr));
        mq2_lineEdit->setText(QApplication::translate("GraduateWidget", "17", nullptr));
        xxxx_label->setText(QApplication::translate("GraduateWidget", "\351\242\204\347\225\231\346\216\245\345\217\243", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Page1), QApplication::translate("GraduateWidget", " \346\225\260\346\215\256", nullptr));
        window_close->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        window_label->setText(QApplication::translate("GraduateWidget", "\347\252\227\346\210\267", nullptr));
        window_state->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        window_open->setText(QApplication::translate("GraduateWidget", "\345\274\200", nullptr));
        door_close->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        door_label->setText(QApplication::translate("GraduateWidget", "\351\227\250", nullptr));
        door_state->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        door_open->setText(QApplication::translate("GraduateWidget", "\345\274\200", nullptr));
        washing_close->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        washing_label->setText(QApplication::translate("GraduateWidget", " \346\264\227\350\241\243\346\234\272", nullptr));
        washing_state->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        washing_open->setText(QApplication::translate("GraduateWidget", "\345\274\200", nullptr));
        air_close->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        air_label->setText(QApplication::translate("GraduateWidget", "\347\251\272\350\260\203", nullptr));
        air_state->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        air_open->setText(QApplication::translate("GraduateWidget", "\345\274\200", nullptr));
        led_close->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        led_label->setText(QApplication::translate("GraduateWidget", " \347\201\257", nullptr));
        led_state->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        led_open->setText(QApplication::translate("GraduateWidget", "\345\274\200", nullptr));
        heater_close->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        heater_label->setText(QApplication::translate("GraduateWidget", " \347\203\255\346\260\264\345\231\250", nullptr));
        heater_state->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        heater_open->setText(QApplication::translate("GraduateWidget", "\345\274\200", nullptr));
        buzzer_close->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        buzzer_label->setText(QApplication::translate("GraduateWidget", "\350\255\246\346\212\245\345\231\250", nullptr));
        buzzer_state->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        buzzer_open->setText(QApplication::translate("GraduateWidget", "\345\274\200", nullptr));
        STM32_close->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        STM32_label->setText(QApplication::translate("GraduateWidget", "\350\207\252\345\212\250", nullptr));
        STM32_state->setText(QApplication::translate("GraduateWidget", "\345\205\263", nullptr));
        STM32_open->setText(QApplication::translate("GraduateWidget", "\345\274\200", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(page2), QApplication::translate("GraduateWidget", " \346\216\247\345\210\266", nullptr));
        send_label->setText(QApplication::translate("GraduateWidget", "\344\277\241\346\201\257\357\274\232", nullptr));
        send_data->setText(QApplication::translate("GraduateWidget", "\344\275\240\345\245\275", nullptr));
        send_pushButton->setText(QApplication::translate("GraduateWidget", " \345\217\221\351\200\201", nullptr));
        send_label_2->setText(QApplication::translate("GraduateWidget", "0x5a 0xa5 \350\212\202\347\202\271 \347\261\273\345\236\213 \350\256\276\345\244\207\347\240\201 \345\221\275\344\273\244 \351\225\277\345\272\246 \346\240\241\351\252\214 0x0d 0x0a", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Page3), QApplication::translate("GraduateWidget", " \345\217\221\351\200\201", nullptr));
        weekday_time->setText(QApplication::translate("GraduateWidget", "23:58:00", nullptr));
        weekday_pushButton->setText(QApplication::translate("GraduateWidget", "\345\267\245\344\275\234\346\227\245", nullptr));
        weekday_close->setText(QApplication::translate("GraduateWidget", " \345\205\263\351\227\255", nullptr));
        weekday_washing->setText(QApplication::translate("GraduateWidget", "\346\264\227\350\241\243\346\234\272", nullptr));
        weekday_air->setText(QApplication::translate("GraduateWidget", "\347\251\272\350\260\203", nullptr));
        weekday_heater->setText(QApplication::translate("GraduateWidget", "\347\203\255\346\260\264\345\231\250", nullptr));
        weekday_clock->setText(QApplication::translate("GraduateWidget", " \351\227\271\351\222\237", nullptr));
        restday_time->setText(QApplication::translate("GraduateWidget", "7:30:00", nullptr));
        restday_pushButton->setText(QApplication::translate("GraduateWidget", " \344\274\221\346\201\257\346\227\245", nullptr));
        restday_close->setText(QApplication::translate("GraduateWidget", " \345\205\263\351\227\255", nullptr));
        restday_washing->setText(QApplication::translate("GraduateWidget", "\346\264\227\350\241\243\346\234\272", nullptr));
        restday_air->setText(QApplication::translate("GraduateWidget", "\347\251\272\350\260\203", nullptr));
        restday_heater->setText(QApplication::translate("GraduateWidget", "\347\203\255\346\260\264\345\231\250", nullptr));
        restday_clock->setText(QApplication::translate("GraduateWidget", " \351\227\271\351\222\237", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Page4), QApplication::translate("GraduateWidget", "\346\250\241\345\274\217", nullptr));
        setting_pushButton->setText(QApplication::translate("GraduateWidget", "\350\256\276\347\275\256", nullptr));
        washing_label_1->setText(QApplication::translate("GraduateWidget", "\346\264\227\350\241\243\346\234\272", nullptr));
        washing_lineEdit_1->setText(QApplication::translate("GraduateWidget", "1500", nullptr));
        heater_label_1->setText(QApplication::translate("GraduateWidget", "\347\203\255\346\260\264\345\231\250", nullptr));
        air_lineEdit_1->setText(QApplication::translate("GraduateWidget", "1500", nullptr));
        air_label_1->setText(QApplication::translate("GraduateWidget", "\347\251\272\350\260\203", nullptr));
        heater_lineEdit_1->setText(QApplication::translate("GraduateWidget", "1500", nullptr));
        maxkw_label->setText(QApplication::translate("GraduateWidget", "\346\234\200\345\244\247\345\212\237\347\216\207", nullptr));
        maxkw_lineEdit->setText(QApplication::translate("GraduateWidget", "6000", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Page5), QApplication::translate("GraduateWidget", "\350\256\276\347\275\256", nullptr));
        close_Butten->setText(QApplication::translate("GraduateWidget", "\351\200\200\345\207\272", nullptr));
        m_hide_Butten->setText(QApplication::translate("GraduateWidget", "\351\232\220\350\227\217", nullptr));
        week_1->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class GraduateWidget: public Ui_GraduateWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRADUATEWIDGET_H
