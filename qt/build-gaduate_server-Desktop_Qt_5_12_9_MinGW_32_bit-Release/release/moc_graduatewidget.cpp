/****************************************************************************
** Meta object code from reading C++ file 'graduatewidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../gaduate_server/graduatewidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'graduatewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_GraduateWidget_t {
    QByteArrayData data[43];
    char stringdata0[928];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GraduateWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GraduateWidget_t qt_meta_stringdata_GraduateWidget = {
    {
QT_MOC_LITERAL(0, 0, 14), // "GraduateWidget"
QT_MOC_LITERAL(1, 15, 22), // "on_star_server_clicked"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 22), // "on_stop_server_clicked"
QT_MOC_LITERAL(4, 62, 15), // "tcp_New_Connect"
QT_MOC_LITERAL(5, 78, 8), // "MSGError"
QT_MOC_LITERAL(6, 87, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(7, 116, 13), // "tcp_Read_Data"
QT_MOC_LITERAL(8, 130, 13), // "udp_Read_Data"
QT_MOC_LITERAL(9, 144, 11), // "update_time"
QT_MOC_LITERAL(10, 156, 11), // "update_week"
QT_MOC_LITERAL(11, 168, 25), // "on_message_Button_clicked"
QT_MOC_LITERAL(12, 194, 22), // "on_window_open_clicked"
QT_MOC_LITERAL(13, 217, 23), // "on_window_close_clicked"
QT_MOC_LITERAL(14, 241, 20), // "on_door_open_clicked"
QT_MOC_LITERAL(15, 262, 21), // "on_door_close_clicked"
QT_MOC_LITERAL(16, 284, 23), // "on_washing_open_clicked"
QT_MOC_LITERAL(17, 308, 24), // "on_washing_close_clicked"
QT_MOC_LITERAL(18, 333, 19), // "on_air_open_clicked"
QT_MOC_LITERAL(19, 353, 20), // "on_air_close_clicked"
QT_MOC_LITERAL(20, 374, 22), // "on_heater_open_clicked"
QT_MOC_LITERAL(21, 397, 23), // "on_heater_close_clicked"
QT_MOC_LITERAL(22, 421, 19), // "on_led_open_clicked"
QT_MOC_LITERAL(23, 441, 20), // "on_led_close_clicked"
QT_MOC_LITERAL(24, 462, 23), // "on_video_Button_clicked"
QT_MOC_LITERAL(25, 486, 22), // "on_buzzer_open_clicked"
QT_MOC_LITERAL(26, 509, 23), // "on_buzzer_close_clicked"
QT_MOC_LITERAL(27, 533, 29), // "on_weekday_pushButton_clicked"
QT_MOC_LITERAL(28, 563, 26), // "on_weekday_washing_clicked"
QT_MOC_LITERAL(29, 590, 7), // "checked"
QT_MOC_LITERAL(30, 598, 22), // "on_weekday_air_clicked"
QT_MOC_LITERAL(31, 621, 25), // "on_weekday_heater_clicked"
QT_MOC_LITERAL(32, 647, 24), // "on_weekday_clock_clicked"
QT_MOC_LITERAL(33, 672, 24), // "on_weekday_close_clicked"
QT_MOC_LITERAL(34, 697, 21), // "on_STM32_open_clicked"
QT_MOC_LITERAL(35, 719, 22), // "on_STM32_close_clicked"
QT_MOC_LITERAL(36, 742, 26), // "on_restday_washing_clicked"
QT_MOC_LITERAL(37, 769, 22), // "on_restday_air_clicked"
QT_MOC_LITERAL(38, 792, 25), // "on_restday_heater_clicked"
QT_MOC_LITERAL(39, 818, 24), // "on_restday_clock_clicked"
QT_MOC_LITERAL(40, 843, 29), // "on_restday_pushButton_clicked"
QT_MOC_LITERAL(41, 873, 24), // "on_restday_close_clicked"
QT_MOC_LITERAL(42, 898, 29) // "on_setting_pushButton_clicked"

    },
    "GraduateWidget\0on_star_server_clicked\0"
    "\0on_stop_server_clicked\0tcp_New_Connect\0"
    "MSGError\0QAbstractSocket::SocketError\0"
    "tcp_Read_Data\0udp_Read_Data\0update_time\0"
    "update_week\0on_message_Button_clicked\0"
    "on_window_open_clicked\0on_window_close_clicked\0"
    "on_door_open_clicked\0on_door_close_clicked\0"
    "on_washing_open_clicked\0"
    "on_washing_close_clicked\0on_air_open_clicked\0"
    "on_air_close_clicked\0on_heater_open_clicked\0"
    "on_heater_close_clicked\0on_led_open_clicked\0"
    "on_led_close_clicked\0on_video_Button_clicked\0"
    "on_buzzer_open_clicked\0on_buzzer_close_clicked\0"
    "on_weekday_pushButton_clicked\0"
    "on_weekday_washing_clicked\0checked\0"
    "on_weekday_air_clicked\0on_weekday_heater_clicked\0"
    "on_weekday_clock_clicked\0"
    "on_weekday_close_clicked\0on_STM32_open_clicked\0"
    "on_STM32_close_clicked\0"
    "on_restday_washing_clicked\0"
    "on_restday_air_clicked\0on_restday_heater_clicked\0"
    "on_restday_clock_clicked\0"
    "on_restday_pushButton_clicked\0"
    "on_restday_close_clicked\0"
    "on_setting_pushButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GraduateWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      39,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  209,    2, 0x08 /* Private */,
       3,    0,  210,    2, 0x08 /* Private */,
       4,    0,  211,    2, 0x08 /* Private */,
       5,    1,  212,    2, 0x08 /* Private */,
       7,    0,  215,    2, 0x08 /* Private */,
       8,    0,  216,    2, 0x08 /* Private */,
       9,    0,  217,    2, 0x08 /* Private */,
      10,    0,  218,    2, 0x08 /* Private */,
      11,    0,  219,    2, 0x08 /* Private */,
      12,    0,  220,    2, 0x08 /* Private */,
      13,    0,  221,    2, 0x08 /* Private */,
      14,    0,  222,    2, 0x08 /* Private */,
      15,    0,  223,    2, 0x08 /* Private */,
      16,    0,  224,    2, 0x08 /* Private */,
      17,    0,  225,    2, 0x08 /* Private */,
      18,    0,  226,    2, 0x08 /* Private */,
      19,    0,  227,    2, 0x08 /* Private */,
      20,    0,  228,    2, 0x08 /* Private */,
      21,    0,  229,    2, 0x08 /* Private */,
      22,    0,  230,    2, 0x08 /* Private */,
      23,    0,  231,    2, 0x08 /* Private */,
      24,    0,  232,    2, 0x08 /* Private */,
      25,    0,  233,    2, 0x08 /* Private */,
      26,    0,  234,    2, 0x08 /* Private */,
      27,    0,  235,    2, 0x08 /* Private */,
      28,    1,  236,    2, 0x08 /* Private */,
      30,    1,  239,    2, 0x08 /* Private */,
      31,    1,  242,    2, 0x08 /* Private */,
      32,    1,  245,    2, 0x08 /* Private */,
      33,    0,  248,    2, 0x08 /* Private */,
      34,    0,  249,    2, 0x08 /* Private */,
      35,    0,  250,    2, 0x08 /* Private */,
      36,    1,  251,    2, 0x08 /* Private */,
      37,    1,  254,    2, 0x08 /* Private */,
      38,    1,  257,    2, 0x08 /* Private */,
      39,    1,  260,    2, 0x08 /* Private */,
      40,    0,  263,    2, 0x08 /* Private */,
      41,    0,  264,    2, 0x08 /* Private */,
      42,    0,  265,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void GraduateWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<GraduateWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_star_server_clicked(); break;
        case 1: _t->on_stop_server_clicked(); break;
        case 2: _t->tcp_New_Connect(); break;
        case 3: _t->MSGError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 4: _t->tcp_Read_Data(); break;
        case 5: _t->udp_Read_Data(); break;
        case 6: _t->update_time(); break;
        case 7: _t->update_week(); break;
        case 8: _t->on_message_Button_clicked(); break;
        case 9: _t->on_window_open_clicked(); break;
        case 10: _t->on_window_close_clicked(); break;
        case 11: _t->on_door_open_clicked(); break;
        case 12: _t->on_door_close_clicked(); break;
        case 13: _t->on_washing_open_clicked(); break;
        case 14: _t->on_washing_close_clicked(); break;
        case 15: _t->on_air_open_clicked(); break;
        case 16: _t->on_air_close_clicked(); break;
        case 17: _t->on_heater_open_clicked(); break;
        case 18: _t->on_heater_close_clicked(); break;
        case 19: _t->on_led_open_clicked(); break;
        case 20: _t->on_led_close_clicked(); break;
        case 21: _t->on_video_Button_clicked(); break;
        case 22: _t->on_buzzer_open_clicked(); break;
        case 23: _t->on_buzzer_close_clicked(); break;
        case 24: _t->on_weekday_pushButton_clicked(); break;
        case 25: _t->on_weekday_washing_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: _t->on_weekday_air_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 27: _t->on_weekday_heater_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: _t->on_weekday_clock_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 29: _t->on_weekday_close_clicked(); break;
        case 30: _t->on_STM32_open_clicked(); break;
        case 31: _t->on_STM32_close_clicked(); break;
        case 32: _t->on_restday_washing_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 33: _t->on_restday_air_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 34: _t->on_restday_heater_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 35: _t->on_restday_clock_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 36: _t->on_restday_pushButton_clicked(); break;
        case 37: _t->on_restday_close_clicked(); break;
        case 38: _t->on_setting_pushButton_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject GraduateWidget::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_GraduateWidget.data,
    qt_meta_data_GraduateWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *GraduateWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GraduateWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_GraduateWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int GraduateWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 39)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 39;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 39)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 39;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
