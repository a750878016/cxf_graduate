#ifndef GRADUATEWIDGET_H
#define GRADUATEWIDGET_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>
#include <QUdpSocket>   //UDP
#include <QtNetwork>
#include <QMap>
//#include <qmap.h>
//#include <string.h>
#include <QString>
#include <opencv2/opencv.hpp>
#include <QBuffer>
#include<QImageReader>
#include <QTime>
#include <QTimer>
#include <QDateTime>
#include "ui_graduatewidget.h"

#include "QMessageBox"
#include <QPushButton>
#include <QDebug>
#include "qtextcodec.h"

#define BUF_SIZE 16     //数据长度


/*这是Designer使用了pimpl手法，pImpl手法主要作用是解开类的使用接口和实现的耦合，即为了减少各个源文件之间的联系*/
QT_BEGIN_NAMESPACE
namespace Ui {
    class GraduateWidget;
}
QT_END_NAMESPACE


class GraduateWidget : public QWidget   //继承自QWidget
{
    Q_OBJECT                            //一个宏，可以使用QT的信号与槽机制

public:         //公开
    GraduateWidget(QWidget *parent = nullptr);  //构造函数声明；GraduateWidget不会作为任何控件的子部件
    ~GraduateWidget();                  //析构函数声明

signals:        //自定义信号 ，必须使用signals声明

private slots:  //自定义槽函数

    void on_star_server_clicked();  //开启服务器button槽函数

    void on_stop_server_clicked();  //关闭服务器button槽函数

    void tcp_New_Connect();         //tcp客户端建立一个新连接

    void MSGError(QAbstractSocket::SocketError);//tcp客户端断开连接反馈

    void tcp_Read_Data();           //tcp读取数据

    void udp_Read_Data();           //udp读取数据

    void update_time();             //更新时间

    void update_week();             //更新星期，检测总功率

    void on_message_Button_clicked();   //清空消息框

    void on_window_open_clicked();  //打开窗户

    void on_window_close_clicked(); //关

    void on_door_open_clicked();    //开门

    void on_door_close_clicked();   //关

    void on_washing_open_clicked(); //开洗衣机

    void on_washing_close_clicked();//关

    void on_air_open_clicked();     //开空调

    void on_air_close_clicked();    //关

    void on_heater_open_clicked();  //开热水器

    void on_heater_close_clicked(); //关

    void on_led_open_clicked();     //开灯

    void on_led_close_clicked();    //关灯

    void on_video_Button_clicked(); //采集图片

    void on_buzzer_open_clicked();  //蜂鸣器开

    void on_buzzer_close_clicked(); //关


    void on_weekday_pushButton_clicked();   //工作日模式的设置

    void on_weekday_washing_clicked(bool checked);  //洗衣机复选框

    void on_weekday_air_clicked(bool checked);      //空调复选框

    void on_weekday_heater_clicked(bool checked);   //热水器复选框

    void on_weekday_clock_clicked(bool checked);    //闹钟复选框

    void on_weekday_close_clicked();        //关闭工作日模式设置

    void on_STM32_open_clicked();           //自动模式打开

    void on_STM32_close_clicked();          //关闭自动模式

    void on_restday_washing_clicked(bool checked);

    void on_restday_air_clicked(bool checked);

    void on_restday_heater_clicked(bool checked);

    void on_restday_clock_clicked(bool checked);

    void on_restday_pushButton_clicked();   //休息日模式的设置

    void on_restday_close_clicked();        //关闭休息日模式设置

    void on_setting_pushButton_clicked();   //功率设置

private:        //私有
    Ui::GraduateWidget *ui;     //使 ui 布局控制和其他的控制代码分离

    QString Tcp_Port = "8088";  //TCP端口号设置
    QString Udp_Port = "8099";  //UDP端口号设置

    //创建QMap实例, 第一个参数为QString类型的键，第二个参数为QTcpSocket类型的值
    QMap<QString,QTcpSocket *> allClientMap;

    QTcpServer *Tcp_server;     //Qt tcp的server
    QTcpSocket *Tcp_socket;     //QT TCP
    QUdpSocket *Udp_Socket;     //Qt UDP

    QTimer *pTimer;             //定时器对象，用于时间显示
    QTimer *pTimer_week;        //定时器对象，用于星期显示

    QTime baseTime;             //用于闹钟，记录开启闹钟那时的时间
    QTime baseTime_1;           //用于闹钟，记录开启闹钟那时的时间
    QTime currTime;             //用于闹钟，记录当前时间

    QString time;               //信息时间
    QString timeStr1;
    QString time_h;             //时间显示
    QString time_m;
    QString time_s;
    QString day_y;              //时间显示
    QString day_m;
    QString day_d;

    int flag1_=0;               //客户端个数
    int len_array;              //接收信息时QByteArray的长度

    uchar clock_flag=0;         //闹钟标志位
    uchar clock_flag_1=0;       //闹钟标志位
    int h,h1;
    int m,m1;
    int s,s1;
    //int t1;

    quint64 udp_data_size;      //udp接收数据的长度

    uchar recive_data[BUF_SIZE];    //接收数据数组
    uchar send_data[BUF_SIZE];      //发送接收数组

    uchar weekday_device[5];        //工作日设备码
    uchar restday_device[5];        //休息日设备码

    /*功率设置*/
    uint KW_H[5];                   //保存设备的功率；null、洗衣机、空调、热水器
    uint KW_MAX=0;                  //最大功率
    uint KW=0;                      //当前功率值
    char device_states[10];        //记录设备当前的状态；null、洗衣机、空调、热水器、门、窗、蜂鸣器、灯、STM32
    char device_states_1[10];      //记录设备之前的状态。
    uchar wait_task[10];            //存放正在等待的设备

    void clock_task();              //工作日定时任务
    void clock_task_1();            //休息日定时任务

    void sleep(unsigned int msec);  //延迟函数

};
#endif // GRADUATEWIDGET_H














