QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Server

TEMPLATE = app

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    main.cpp \
    graduatewidget.cpp

HEADERS += \
    graduatewidget.h

FORMS += \
    graduatewidget.ui

# opencv
INCLUDEPATH += D:\opencv\opencv4.5.1\opencv\opencv-build\install\include
LIBS += D:\opencv\opencv4.5.1\opencv\opencv-build\lib\libopencv_*.a


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RC_ICONS = ico/a0xhq-3ridx-001.ico  # 图标

