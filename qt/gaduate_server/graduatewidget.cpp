#include "graduatewidget.h"


GraduateWidget::GraduateWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::GraduateWidget)
{
    ui->setupUi(this);

    ui->tcp_port->setText(Tcp_Port);            //设置服务器默认端口号
    ui->udp_port->setText(Udp_Port);            //设置服务器默认端口号

    /*获取本地ip地址*/
    QList<QHostAddress> list =QNetworkInterface::allAddresses();
    foreach (QHostAddress address, list)
    {
        if(address.protocol() ==QAbstractSocket::IPv4Protocol)  //我们使用IPv4地址
        {
            if(address.toString().left(10)=="192.168.1.")
                ui->ip_lineEdit->setText(address.toString());
            else if(address.toString().left(10)=="192.168.4.")
                ui->ip_lineEdit->setText(address.toString());
            else if(address.toString().left(11)=="192.168.43.")
                ui->ip_lineEdit->setText(address.toString());
        }
    }

    memset(weekday_device,0,4);     //工作日任务
    memset(restday_device,0,4);     //休息日任务
    memset(device_states,0,10);     //设备状态，初始化为0
    memset(device_states_1,0,10);     //设备状态，初始化为0
    memset(wait_task,10,10);        //设备等待

    connect(ui->close_Butten, &QPushButton::clicked,this, [=](){
        this->close();
    });
    connect(ui->m_hide_Butten, &QPushButton::clicked,this, [=](){
        this->lower();
    });

    /*初始化定时器，用于更新时间*/
    this->pTimer = new QTimer;
    connect(this->pTimer,&QTimer::timeout,this,&GraduateWidget::update_time);
    pTimer->start(1000);

    /*初始化定时器，用于更新星期*/
    this->pTimer_week = new QTimer;
    connect(this->pTimer_week,&QTimer::timeout,this,&GraduateWidget::update_week);
    pTimer_week->start(2000);

    GraduateWidget::on_setting_pushButton_clicked();

    //控制按键失能
    ui->window_open->setEnabled(false);
    ui->window_close->setEnabled(false);
    ui->door_open->setEnabled(false);
    ui->door_close->setEnabled(false);
    ui->washing_open->setEnabled(false);
    ui->washing_close->setEnabled(false);
    ui->air_open->setEnabled(false);
    ui->air_close->setEnabled(false);
    ui->heater_open->setEnabled(false);
    ui->heater_close->setEnabled(false);
    ui->led_open->setEnabled(false);
    ui->led_close->setEnabled(false);
    ui->buzzer_open->setEnabled(false);
    ui->buzzer_close->setEnabled(false);
    ui->video_Button->setEnabled(false);
    ui->STM32_open->setEnabled(false);
    ui->STM32_close->setEnabled(false);
    ui->weekday_pushButton->setEnabled(false);
    ui->restday_pushButton->setEnabled(false);
    ui->weekday_close->setEnabled(false);
    ui->restday_close->setEnabled(false);

    //清空
    ui->washing_state->setText("未知");
    ui->air_state->setText("未知");
    ui->heater_state->setText("未知");
    ui->led_state->setText("未知");
    ui->led_state->setText("未知");
    ui->door_state->setText("未知");
    ui->window_state->setText("未知");
    ui->buzzer_state->setText("未知");
    ui->STM32_state->setText("未知");
}

GraduateWidget::~GraduateWidget()
{
    delete ui;
}

/*开启服务器button槽函数*/
void GraduateWidget::on_star_server_clicked()
{
    ui->message_textEdit->setText("");          //清理消息框
    Tcp_server = new QTcpServer();              //TCP,处理消息
    Udp_Socket = new QUdpSocket();

    int tcp_port = ui->tcp_port->text().toInt();    //从TCP输入框获取端口号
    int udp_port = ui->udp_port->text().toInt();    //从UDP输入框获取端口号

    /*连接信号槽，Tcp申请连接*/
    connect(Tcp_server,&QTcpServer::newConnection,this,&GraduateWidget::tcp_New_Connect);

    /*监听指定的端口，失败输出错误*/
    if(!Tcp_server->listen(QHostAddress::Any, tcp_port))
    {
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("GBK"));
        QMessageBox::information(this,tr("TCP客户端_1"),tr("打开TCP服务器失败！"),QMessageBox::Yes);
        return;
    }

    /*获取本地ip地址*/
    QList<QHostAddress> list =QNetworkInterface::allAddresses();
    foreach (QHostAddress address, list)
    {
        if(address.protocol() ==QAbstractSocket::IPv4Protocol)  //我们使用IPv4地址
        {
            /*打印消息*/
            time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
            ui->message_textEdit->append(time+address.toString()+"\n");
        }
    }

    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("TCP服务器启动成功！\n"));


    /*开启UDP*/
    if(!Udp_Socket->bind(QHostAddress(QHostAddress::Any), udp_port))
    {
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("GBK"));
        QMessageBox::information(this,tr("TCP客户端_1"),tr("打开UDP服务器失败！"),QMessageBox::Yes);
        return;
    }
    /*只要 UDP 数据报到达指定的地址和端口，就会触发 readyRead() 信号，此时可在槽函数中读取数据*/
    connect(Udp_Socket, SIGNAL(readyRead()), this, SLOT(udp_Read_Data()));
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("UDP服务器启动成功！\n"));


    ui->star_server->setEnabled(false); //启动服务器button失能
    ui->stop_server->setEnabled(true);  //关闭服务器button使能

    //初始化
    ui->rain_lineEdit->setText("00");
    ui->wendu_lineEdit->setText("00");
    ui->shidu_lineEdit->setText("00");
    ui->mq2_lineEdit->setText("00");
    ui->light_lineEdit->setText("00");
}

void GraduateWidget::on_stop_server_clicked()   //关闭服务器button槽函数
{
    Tcp_server->deleteLater();  //取消监听

    Tcp_server->close();
    Udp_Socket->close();

    ui->star_server->setEnabled(true);  //启动服务器button使能
    ui->stop_server->setEnabled(false); //关闭服务器button失能

    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("TCP和UDP服务器关闭！\n"));

    flag1_=0;   //tcp客户端数置0
    allClientMap.clear();

    //控制按键失能
    ui->window_open->setEnabled(false);
    ui->window_close->setEnabled(false);
    ui->door_open->setEnabled(false);
    ui->door_close->setEnabled(false);
    ui->washing_open->setEnabled(false);
    ui->washing_close->setEnabled(false);
    ui->air_open->setEnabled(false);
    ui->air_close->setEnabled(false);
    ui->heater_open->setEnabled(false);
    ui->heater_close->setEnabled(false);
    ui->led_open->setEnabled(false);
    ui->led_close->setEnabled(false);
    ui->buzzer_open->setEnabled(false);
    ui->buzzer_close->setEnabled(false);
    ui->video_Button->setEnabled(false);
    ui->STM32_open->setEnabled(false);
    ui->STM32_close->setEnabled(false);
    ui->weekday_pushButton->setEnabled(false);
    ui->restday_pushButton->setEnabled(false);
    ui->weekday_close->setEnabled(false);
    ui->restday_close->setEnabled(false);

    clock_flag = 0;     //关闭定时任务
    clock_flag_1 = 0;

}

void GraduateWidget::tcp_New_Connect()  //tcp客户端建立一个新连接
{
    //获取客户端连接
    Tcp_socket = Tcp_server->nextPendingConnection();
    //连接QTcpSocket的信号槽，以读取新数据
    connect(Tcp_socket, &QTcpSocket::readyRead, this, &GraduateWidget::tcp_Read_Data);

    //断开连接
    connect(Tcp_socket, SIGNAL(error(QAbstractSocket::SocketError)),this, SLOT(MSGError(QAbstractSocket::SocketError)));

    //绑定TCP客户端和消息键值，因为不限定一个客户端
    allClientMap.insert(QString("TCP客户端_")+QString::number(flag1_),Tcp_socket);
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("TCP客户端_")+QString::number(flag1_)+QString("-连接成功！\n"));

    if(flag1_==0)
    {
        //控制按键使能
        ui->window_open->setEnabled(true);
        ui->window_close->setEnabled(true);
        ui->door_open->setEnabled(true);
        ui->door_close->setEnabled(true);
        ui->washing_open->setEnabled(true);
        ui->washing_close->setEnabled(true);
        ui->air_open->setEnabled(true);
        ui->air_close->setEnabled(true);
        ui->heater_open->setEnabled(true);
        ui->heater_close->setEnabled(true);
        ui->led_open->setEnabled(true);
        ui->led_close->setEnabled(true);
        ui->buzzer_open->setEnabled(true);
        ui->buzzer_close->setEnabled(true);
        ui->video_Button->setEnabled(true);
        ui->STM32_open->setEnabled(true);
        ui->STM32_close->setEnabled(true);
        ui->weekday_pushButton->setEnabled(true);
        ui->restday_pushButton->setEnabled(true);
        ui->weekday_close->setEnabled(true);
        ui->restday_close->setEnabled(true);
    }
    flag1_++;
}

void GraduateWidget::tcp_Read_Data()            //tcp读取数据
{
    QTcpSocket *who = (QTcpSocket *)this->sender();
    QByteArray buffer;          //提供一个字节数组,可用于存储原始字节（包括“\ 0” ）和传统的8位 “\ 0” 端接字符串
    buffer = who->readAll();    //读
    len_array = buffer.size();  //长度

    if(who==allClientMap["TCP客户端_0"])
    {
        memcpy(recive_data,buffer,len_array);   //存放数据
        //数据帧
        if(recive_data[0]==0xa5&&recive_data[1]==0x5a &&recive_data[3]==0x02)
        {
            //接收数据
            switch (recive_data[4]) //设备码
            {
            case 0x01:              //雨滴
                ui->rain_lineEdit->setText(QString::number(recive_data[5]-1));
                break;

            case 0x02:              //温湿度
                ui->wendu_lineEdit->setText(QString::number(recive_data[5]));
                ui->shidu_lineEdit->setText(QString::number(recive_data[6]));
                break;
            case 0x03:              //MQ2
                ui->mq2_lineEdit->setText(QString::number(recive_data[5]));
                break;
            case 0x04:              //光敏
                ui->light_lineEdit->setText(QString::number(recive_data[5]-1));
                break;

            case 0x06:              //摄像头
                if(recive_data[5]==0x02)
                {
                    /*打印消息*/
                    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
                    ui->message_textEdit->append(time+QString("采集图片成功！\n"));
                }
                else if(recive_data[5]==0x03)
                {
                    /*打印消息*/
                    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
                    ui->message_textEdit->append(time+QString("采集图片失败！\n"));
                }
                break;
            default:
                break;
            }
        }
        //设备反馈控制帧
        else if(recive_data[0]==0xa5&&recive_data[1]==0x5a &&recive_data[3]==0x03)
        {
            /*获取时间*/
            time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
            //接收数据
            switch (recive_data[4]) //设备码
            {
            case 0x01:              //继电器
                switch (recive_data[2]) //设备码
                {
                case 0x01:          //洗衣机
                    if(recive_data[5]==1)
                    {
                        ui->washing_state->setText("开");
                        ui->washing_open->setEnabled(false);
                        ui->washing_close->setEnabled(true);
                        ui->message_textEdit->append(time+QString("洗衣机打开成功！\n"));
                        device_states[1] = 1;
                    }else if(recive_data[5]==2)
                    {
                        ui->washing_state->setText("关");
                        ui->washing_open->setEnabled(true);
                        ui->washing_close->setEnabled(false);
                        ui->message_textEdit->append(time+QString("洗衣机关闭成功！\n"));
                        device_states[1] = -1;
                    }
                    break;

                case 0x02:          //空调
                    if(recive_data[5]==1)
                    {
                        ui->air_state->setText("开");
                        ui->air_open->setEnabled(false);
                        ui->air_close->setEnabled(true);
                        ui->message_textEdit->append(time+QString("空调打开成功！\n"));
                        device_states[2] = 1;
                    }else if(recive_data[5]==2)
                    {
                        ui->air_state->setText("关");
                        ui->air_open->setEnabled(true);
                        ui->air_close->setEnabled(false);
                        ui->message_textEdit->append(time+QString("空调关闭成功！\n"));
                        device_states[2] = -1;
                    }
                    break;

                case 0x03:          //热水器
                    if(recive_data[5]==1)
                    {
                        ui->heater_state->setText("开");
                        ui->heater_open->setEnabled(false);
                        ui->heater_close->setEnabled(true);
                        ui->message_textEdit->append(time+QString("热水器打开成功！\n"));
                        device_states[3] = 1;
                    }else if(recive_data[5]==2)
                    {
                        ui->heater_state->setText("关");
                        ui->heater_open->setEnabled(true);
                        ui->heater_close->setEnabled(false);
                        ui->message_textEdit->append(time+QString("热水器关闭成功！\n"));
                        device_states[3] = -1;
                    }
                    break;

                default:
                    break;
                }
                break;

            case 0x02:              //门。舵机
                if(recive_data[5]==1)
                {
                    ui->door_state->setText("开");
                    ui->door_open->setEnabled(false);
                    ui->door_close->setEnabled(true);
                    ui->message_textEdit->append(time+QString("开门成功！\n"));
                }else if(recive_data[5]==2)
                {
                    ui->door_state->setText("关");
                    ui->door_open->setEnabled(true);
                    ui->door_close->setEnabled(false);
                    ui->message_textEdit->append(time+QString("关门成功！\n"));
                }
                break;

            case 0x03:              //窗户。步进电机
                if(recive_data[5]==1)
                {
                    ui->window_state->setText("开");
                    ui->window_open->setEnabled(false);
                    ui->window_close->setEnabled(true);
                    ui->message_textEdit->append(time+QString("窗户打开成功！\n"));
                }else if(recive_data[5]==2)
                {
                    ui->window_state->setText("关");
                    ui->window_open->setEnabled(true);
                    ui->window_close->setEnabled(false);
                    ui->message_textEdit->append(time+QString("空调关闭成功！\n"));
                }
                break;

            case 0x04:              //警报器。蜂鸣器
                if(recive_data[5]==1)
                {
                    ui->buzzer_state->setText("开");
                    ui->buzzer_open->setEnabled(false);
                    ui->buzzer_close->setEnabled(true);
                    ui->message_textEdit->append(time+QString("蜂鸣器打开成功！\n"));
                }else if(recive_data[5]==2)
                {
                    ui->buzzer_state->setText("关");
                    ui->buzzer_open->setEnabled(true);
                    ui->buzzer_close->setEnabled(false);
                    ui->message_textEdit->append(time+QString("蜂鸣器关闭成功！\n"));
                }
                break;

            case 0x05:          //灯
                if(recive_data[5]==1)
                {
                    ui->led_state->setText("开");
                    ui->led_open->setEnabled(false);
                    ui->led_close->setEnabled(true);
                    ui->message_textEdit->append(time+QString("开灯成功！\n"));
                }else if(recive_data[5]==2)
                {
                    ui->led_state->setText("关");
                    ui->led_open->setEnabled(true);
                    ui->led_close->setEnabled(false);
                    ui->message_textEdit->append(time+QString("关灯成功！\n"));
                }
                break;

            case 0x06:          //STM32
                if(recive_data[5]==1)
                {
                    ui->STM32_state->setText("开");
                    ui->STM32_open->setEnabled(false);
                    ui->STM32_close->setEnabled(true);
                    ui->message_textEdit->append(time+QString("自动功能打开成功！\n"));
                }else if(recive_data[5]==2)
                {
                    ui->STM32_state->setText("关");
                    ui->STM32_open->setEnabled(true);
                    ui->STM32_close->setEnabled(false);
                    ui->message_textEdit->append(time+QString("自动功能关闭成功！\n"));
                }
                break;

            default:
                break;
            }
        }
        //释放
        buffer.clear();
        buffer.squeeze();
    }

}

void GraduateWidget::udp_Read_Data()             //udp读取数据
{
    udp_data_size = Udp_Socket->pendingDatagramSize();

    QByteArray buff;            //用来存储缓冲数据
    buff.resize(udp_data_size);
    QHostAddress adrr ;
    quint16 port;

    Udp_Socket->readDatagram(buff.data(),buff.size(),&adrr,&port);

    QBuffer buffer(&buff);      //用来读写内存缓存
    QImageReader reader(&buffer,"JPEG");    //接收的图片是ipg格式，解码

    QImage image = reader.read();

    //显示图片
    ui->video_show->setPixmap(QPixmap::fromImage(image));
    ui->video_show->resize(image.width(),image.height());
}

//断开报错
void GraduateWidget::MSGError(QAbstractSocket::SocketError)
{
    QTcpSocket *which = (QTcpSocket *)this->sender();
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    if(which == allClientMap["TCP客户端_0"])
      {
        ui->message_textEdit->append(time+QString("[TCP客户端_0] 断开连接\n"));
        allClientMap.erase(allClientMap.begin());   //删除qmap中存放的元素
        qDebug()<<"消息"<<allClientMap;
        flag1_=0;
        //控制按键失能
        ui->window_open->setEnabled(false);
        ui->window_close->setEnabled(false);
        ui->door_open->setEnabled(false);
        ui->door_close->setEnabled(false);
        ui->washing_open->setEnabled(false);
        ui->washing_close->setEnabled(false);
        ui->air_open->setEnabled(false);
        ui->air_close->setEnabled(false);
        ui->heater_open->setEnabled(false);
        ui->heater_close->setEnabled(false);
        ui->led_open->setEnabled(false);
        ui->led_close->setEnabled(false);
        ui->buzzer_open->setEnabled(false);
        ui->buzzer_close->setEnabled(false);
        ui->video_Button->setEnabled(false);
        ui->STM32_open->setEnabled(false);
        ui->STM32_close->setEnabled(false);
        ui->weekday_pushButton->setEnabled(false);
        ui->restday_pushButton->setEnabled(false);
        ui->weekday_close->setEnabled(false);
        ui->restday_close->setEnabled(false);

        clock_flag = 0;     //关闭定时任务
        clock_flag_1 = 0;
      }
}

void GraduateWidget::update_time()             //更新时间
{
    /*
     * 1.点击开始后获取到当前的时间并且赋值给baseTime
     * 2.启动定时器,间隔1s
     * 3.格式化后设置显示
     */

    day_y = QDateTime::currentDateTime().toString("yyyy");
    day_m = QDateTime::currentDateTime().toString("MM");
    day_d = QDateTime::currentDateTime().toString("dd");

    this->ui->day_y->display(day_y);
    this->ui->day_m->display(day_m);
    this->ui->day_d->display(day_d);

    time_h = QDateTime::currentDateTime().toString("hh");
    time_m = QDateTime::currentDateTime().toString("mm");
    time_s = QDateTime::currentDateTime().toString("ss");

    this->ui->time_h->display(time_h);
    this->ui->time_m->display(time_m);
    this->ui->time_s->display(time_s);

    currTime = QTime::currentTime();

    if(clock_flag == 1)     //工作日
    {
        int t1 = this->baseTime.msecsTo(currTime);      //当前时间与设置时间相减
        QTime showTime1(h,m,s);                 //设置倒计时
        showTime1 = showTime1.addMSecs(-t1);    //倒计时
        this->timeStr1 = showTime1.toString("hh:mm:ss");
        this->ui->weekday_lcdNumber->display(timeStr1);
        if(timeStr1=="00:00:00")
        {
            //重新开始
            this->baseTime = QTime::currentTime();  //记录当前的时间
            h = 23;
            m = 59;
            s = 59;
            /*打印消息*/
            time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
            ui->message_textEdit->append(time+"时间到，闹钟开启！\n");
            GraduateWidget::clock_task();   //执行定时任务
            //clock_flag = 0;
        }
        this->timeStr1 = showTime1.toString("hh:");
        this->ui->weekday_lcdNumber_2->display(timeStr1);
    }

    else if(clock_flag_1 == 1)   //休息日
    {
        int t2 = this->baseTime_1.msecsTo(currTime);      //当前时间与设置时间相减
        QTime showTime2(h1,m1,s1);                 //设置倒计时
        showTime2 = showTime2.addMSecs(-t2);    //倒计时
        this->timeStr1 = showTime2.toString("hh:mm:ss");
        this->ui->restday_lcdNumber->display(timeStr1);
        if(timeStr1=="00:00:00")
        {
            //重新开始
            this->baseTime_1 = QTime::currentTime();  //记录当前的时间
            h1 = 23;
            m1 = 59;
            s1 = 59;
            /*打印消息*/
            time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
            ui->message_textEdit->append(time+"时间到，闹钟开启！\n");
            GraduateWidget::clock_task_1();   //执行定时任务
            //clock_flag = 0;
        }
        this->timeStr1 = showTime2.toString("hh:");
        this->ui->restday_lcdNumber_2->display(timeStr1);
    }
}

void GraduateWidget::update_week()             //更新时间
{

    QDateTime dateTime = QDateTime::currentDateTime();
    QLocale locale = QLocale::Chinese;//指定中文显示
    QString strFormat = "dddd";
    QString strDateTime = locale.toString(dateTime, strFormat);
    ui->week_1->setText(strDateTime);          //清理消息框

    if(device_states[1]!=device_states_1[1])    //洗衣机状态发生改变
    {
        KW += KW_H[1]*device_states[1];
        /*打印消息*/
        time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
        ui->message_textEdit->append(time+QString("当前功率：")+QString::number(KW)+QString("!\n"));
    }
    if(device_states[2]!=device_states_1[2])    //空调状态发生改变
    {
        KW += KW_H[2]*device_states[2];
        /*打印消息*/
        time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
        ui->message_textEdit->append(time+QString("当前功率：")+QString::number(KW)+QString("!\n"));
    }
    if(device_states[3]!=device_states_1[3])    //热水器状态发生改变
    {
        KW += KW_H[3]*device_states[3];
        /*打印消息*/
        time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
        ui->message_textEdit->append(time+QString("当前功率：")+QString::number(KW)+QString("!\n"));
    }

    if(KW>KW_MAX)                   //超出最大功率
    {
        /*打印消息*/
        time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
        ui->message_textEdit->append(time+QString("超功率警告， 当前功率：")+QString::number(KW)+QString("!\n"));
        if(KW-KW_H[1]<=KW_MAX && device_states[1]==1)         //洗衣机打开且关闭后总功率低于最大功率
        {
            GraduateWidget::on_washing_close_clicked();
            wait_task[1] = 1;                         //挂起等待任务
        }else if(KW-KW_H[3]<=KW_MAX && device_states[3]==1)   //热水器打开且关闭后总功率低于最大功率
        {
            GraduateWidget::on_heater_close_clicked();
            wait_task[3] = 1;                        //挂起等待任务
        }else
        {
            GraduateWidget::on_washing_close_clicked();
            wait_task[1] = 1;                        //挂起等待任务
            GraduateWidget::sleep(500);              //延迟
            GraduateWidget::on_heater_close_clicked();
            wait_task[3] = 1;                        //挂起等待任务
        }
    }else
    {
        if(KW+KW_H[3]<=KW_MAX && wait_task[3]==1)         //热水器关闭且打开后总功率低于最大功率
        {
            GraduateWidget::on_heater_open_clicked();
            wait_task[3] = 0;                       //注销等待任务
        }else if(KW+KW_H[1]<=KW_MAX && wait_task[1] ==1)
        {
            GraduateWidget::on_washing_open_clicked();          //洗衣机关闭且打开后总功率低于最大功率
            wait_task[1] = 0;                       //注销等待任务
        }
    }

    memset(device_states_1,0,10);
    memcpy(device_states_1,device_states,10);     //保存当前状态

}

//清空消息框
void GraduateWidget::on_message_Button_clicked()
{
    ui->message_textEdit->setText("");          //清理消息框
}


//按键功能函数
//窗户开
void GraduateWidget::on_window_open_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x02;    //节点2
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x03;    //设备码，步进电机03
    send_data[5] = 0x01;    //正转
    send_data[6] = 0xff;    //两圈
    send_data[7] = 0xff;    //填充
    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;

    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();

    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("开窗户！\n"));
}

//关
void GraduateWidget::on_window_close_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x02;    //节点2
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x03;    //设备码，步进电机03
    send_data[5] = 0x02;    //反转
    send_data[6] = 0xff;    //两圈
    send_data[7] = 0xff;    //填充
    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;

    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("关窗户！\n"));
}


//开门
void GraduateWidget::on_door_open_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x01;    //节点1
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x02;    //设备码，舵机是02
    send_data[5] = 0x01;    //90度，开
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("开门！\n"));
}
//关
void GraduateWidget::on_door_close_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x01;    //节点1
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x02;    //设备码，舵机是02
    send_data[5] = 0x02;    //0度关
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("关门！\n"));
}

//打开洗衣机
void GraduateWidget::on_washing_open_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x01;    //节点1
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x01;    //设备码，继电器是01
    send_data[5] = 0x01;    //打开
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("打开洗衣机！\n"));
}

void GraduateWidget::on_washing_close_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x01;    //节点1
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x01;    //设备码，继电器是01
    send_data[5] = 0x02;    //关闭
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("关闭洗衣机！\n"));
}

//开空调
void GraduateWidget::on_air_open_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x02;    //节点2
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x01;    //设备码，继电器是01
    send_data[5] = 0x01;    //打开
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("打开空调！\n"));
}
//关闭
void GraduateWidget::on_air_close_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x02;    //节点2
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x01;    //设备码，继电器是01
    send_data[5] = 0x02;    //关闭
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("关闭空调！\n"));
}

void GraduateWidget::on_heater_open_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x03;    //节点3，协调器
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x01;    //设备码，继电器是01
    send_data[5] = 0x01;    //打开
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("打开热水器！\n"));
}

void GraduateWidget::on_heater_close_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x03;    //节点3
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x01;    //设备码，继电器是01
    send_data[5] = 0x02;    //关闭
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("关闭热水器！\n"));
}

//开灯
void GraduateWidget::on_led_open_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x03;    //协调器
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x05;    //设备码，led是0x05
    send_data[5] = 0x01;    //打开
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("开灯！\n"));
}

void GraduateWidget::on_led_close_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x03;    //节点4
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x05;    //设备码，继电器是01
    send_data[5] = 0x02;    //关闭
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("关灯！\n"));
}

//采集图片
void GraduateWidget::on_video_Button_clicked()
{
    ui->video_Button->setEnabled(false);

    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x05;    //节点5，jetson nano
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x06;    //设备码，摄像头是06
    send_data[5] = 0x01;    //拍照
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("采集一张图片！\n"));
    ui->video_Button->setEnabled(true);
}

//打开蜂鸣器
void GraduateWidget::on_buzzer_open_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x03;    //节点3
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x04;    //设备码，蜂鸣器是4
    send_data[5] = 0x01;    //打开
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("打开报警器！\n"));
}
//关闭
void GraduateWidget::on_buzzer_close_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x03;    //节点3
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x04;    //设备码，蜂鸣器是4
    send_data[5] = 0x02;    //关闭
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("关闭报警器！\n"));
}

void GraduateWidget::on_weekday_pushButton_clicked()
{
    //获取文本框的时间
    QStringList time_List = ui->weekday_time->text().split(":");
    h = time_List[0].toInt();
    m = time_List[1].toInt();
    s = time_List[2].toInt();

    //获取当前时间
    QStringList current_time_List = QDateTime::currentDateTime().toString("hh:mm:ss").split(":");
    int current_h = current_time_List[0].toInt();
    int current_m = current_time_List[1].toInt();
    int current_s = current_time_List[2].toInt();

    if(h<24&&m<60&&s<60)
    {
        //获取倒计时
        if(h>=current_h)             //设置的小时大于现在
            h = h - current_h;      //相减:24-23=1h
        else
            h = 24 - current_h + h; // 24-19+9=14h

        if(m>=current_m)             //设置的分钟大于现在
            m = m - current_m;      //相减:59-43=16m
        else
        {
            m = 60 - current_m + m; //60-50+40=50m
            if(h>0)
                h = h - 1;              //减一小时
            else
                h = 23;

        }

        if(s>=current_s)             //设置的秒钟大于现在
            s = s - current_s;      //相减:40-30=10s
        else
        {
            s = 60 - current_s +s;  //60-50+40=50s
            if(m>0)
                m = m-1;                //减一分钟
            else
            {
                m = 59;
                if(h>0)
                    h = h - 1;              //减一小时
                else
                    h = 23;
            }
        }
        this->baseTime = QTime::currentTime();  //记录当前的时间
        clock_flag = 1;     //开启定时标志
    }

}

//工作日复选框
void GraduateWidget::on_weekday_washing_clicked(bool checked)   //洗衣机
{
    if(checked)
        weekday_device[0] = 1;
    else
        weekday_device[0] = 0;
}

void GraduateWidget::on_weekday_air_clicked(bool checked)       //空调
{
    if(checked)
        weekday_device[1] = 1;
    else
        weekday_device[1] = 0;
}

void GraduateWidget::on_weekday_heater_clicked(bool checked)    //热水器
{
    if(checked)
        weekday_device[2] = 1;
    else
        weekday_device[2] = 0;
}

void GraduateWidget::on_weekday_clock_clicked(bool checked)     //闹钟
{
    if(checked)
        weekday_device[3] = 1;
    else
        weekday_device[3] = 0;
}

void GraduateWidget::on_weekday_close_clicked()
{
    clock_flag = 0;     //关闭定时标志
}

void GraduateWidget::clock_task()   //定时任务
{
    if(weekday_device[3]==1)
        GraduateWidget::on_buzzer_open_clicked();   //打开蜂鸣器
    else if(weekday_device[3]==0)
        GraduateWidget::on_buzzer_close_clicked();  //关闭蜂鸣器
    GraduateWidget::sleep(1000);                    //延迟

    if(weekday_device[0]==1)
        GraduateWidget::on_washing_open_clicked();  //打开洗衣机
    else if(weekday_device[0]==0)
        GraduateWidget::on_washing_close_clicked(); //关闭洗衣机
    GraduateWidget::sleep(1000);                    //延迟

    if(weekday_device[1]==1)
        GraduateWidget::on_air_open_clicked();      //打开空调
    else if(weekday_device[1]==0)
        GraduateWidget::on_air_close_clicked();     //关闭空调
    GraduateWidget::sleep(1000);                    //延迟

    if(weekday_device[2]==1)
        GraduateWidget::on_heater_open_clicked();   //打开热水器
    else if(weekday_device[2]==0)
        GraduateWidget::on_heater_close_clicked();  //关闭热水器

}


void GraduateWidget::on_STM32_open_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x04;    //STM32
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x06;    //设备码，STM32
    send_data[5] = 0x01;    //开启自主控制
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("打开自主控制！\n"));
}

void GraduateWidget::on_STM32_close_clicked()
{
    QByteArray sendTcpData;
    memset(send_data,0,BUF_SIZE);

    send_data[0] = 0xa5;    //帧头
    send_data[1] = 0x5a;
    send_data[2] = 0x04;    //STM32
    send_data[3] = 0x01;    //控制
    send_data[4] = 0x06;    //设备码，STM32
    send_data[5] = 0x02;    //关闭自主控制
    send_data[6] = 0xff;    //
    send_data[7] = 0xff;    //填充

    send_data[8] = 0xff;
    send_data[9] = 0x0e;    //长度,13
    send_data[10] = 0xff;   //奇偶校验
    send_data[11] = 0x0d;   //帧尾
    send_data[12] = 0x0a;
    sendTcpData.resize(13);
    memcpy(sendTcpData.data(),send_data,13);

    allClientMap["TCP客户端_0"]->write(sendTcpData);     //发送
    Tcp_socket->flush();                //释放socket缓存
    sendTcpData.clear();                //清空QByteArray数组
    sendTcpData.squeeze();
    /*打印消息*/
    time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
    ui->message_textEdit->append(time+QString("关闭自主控制！\n"));
}

void GraduateWidget::on_restday_washing_clicked(bool checked)
{
    if(checked)
        restday_device[0] = 1;
    else
        restday_device[0] = 0;
}

void GraduateWidget::on_restday_air_clicked(bool checked)
{
    if(checked)
        restday_device[1] = 1;
    else
        restday_device[1] = 0;
}

void GraduateWidget::on_restday_heater_clicked(bool checked)
{
    if(checked)
        restday_device[2] = 1;
    else
        restday_device[2] = 0;
}

void GraduateWidget::on_restday_clock_clicked(bool checked)
{
    if(checked)
        restday_device[3] = 1;
    else
        restday_device[3] = 0;
}

void GraduateWidget::on_restday_pushButton_clicked()
{
    //获取文本框的时间
    QStringList time_List = ui->restday_time->text().split(":");
    h1 = time_List[0].toInt();
    m1 = time_List[1].toInt();
    s1 = time_List[2].toInt();

    //获取当前时间
    QStringList current_time_List = QDateTime::currentDateTime().toString("hh:mm:ss").split(":");
    int current_h = current_time_List[0].toInt();
    int current_m = current_time_List[1].toInt();
    int current_s = current_time_List[2].toInt();

    if(h1<24&&m1<60&&s1<60)
    {
        //获取倒计时
        if(h1>=current_h)             //设置的小时大于现在
            h1 = h1 - current_h;      //相减:24-23=1h
        else
            h1 = 24 - current_h + h1; // 24-19+9=14h

        if(m1>=current_m)             //设置的分钟大于现在
            m1 = m1 - current_m;      //相减:59-43=16m
        else
        {
            m1 = 60 - current_m + m1; //60-50+40=50m
            if(h1>0)
                h1 = h1 - 1;              //减一小时
            else
                h1 = 23;

        }

        if(s1>=current_s)             //设置的秒钟大于现在
            s1 = s1 - current_s;      //相减:40-30=10s
        else
        {
            s1 = 60 - current_s +s1;  //60-50+40=50s
            if(m1>0)
                m1 = m1-1;                //减一分钟
            else
            {
                m1 = 59;
                if(h1>0)
                    h1 = h1 - 1;              //减一小时
                else
                    h1 = 23;
            }
        }
        this->baseTime_1 = QTime::currentTime();  //记录当前的时间
        clock_flag_1 = 1;     //开启定时标志
    }
}

void GraduateWidget::on_restday_close_clicked()
{
    clock_flag_1 = 0;     //关闭定时标志
}

void GraduateWidget::clock_task_1()   //定时任务
{
    if(restday_device[3]==1)
        GraduateWidget::on_buzzer_open_clicked();   //打开蜂鸣器
    else if(restday_device[3]==0)
        GraduateWidget::on_buzzer_close_clicked();  //关闭蜂鸣器
    GraduateWidget::sleep(1000);                    //延迟

    if(restday_device[0]==1)
        GraduateWidget::on_washing_open_clicked();  //打开洗衣机
    else if(restday_device[0]==0)
        GraduateWidget::on_washing_close_clicked(); //关闭洗衣机
    GraduateWidget::sleep(1000);                    //延迟

    if(restday_device[1]==1)
        GraduateWidget::on_air_open_clicked();      //打开空调
    else if(restday_device[1]==0)
        GraduateWidget::on_air_close_clicked();     //关闭空调
    GraduateWidget::sleep(1000);                    //延迟

    if(restday_device[2]==1)
        GraduateWidget::on_heater_open_clicked();   //打开热水器
    else if(restday_device[2]==0)
        GraduateWidget::on_heater_close_clicked();  //关闭热水器

}

//延时功能
void GraduateWidget::sleep(unsigned int msec)
{
    QTime reachTime = QTime::currentTime().addMSecs(msec);  //获取当前时间
    while(QTime::currentTime()<reachTime)                   //循环等待
    {
    //如果当前的系统时间尚未达到我们设定的时刻，就让Qt的应用程序类执行默认的处理，
    //以使程序仍处于响应状态。一旦到达了我们设定的时刻，就跳出该循环，继续执行后面的语句。
        QApplication::processEvents(QEventLoop::AllEvents,100);
    }
}

void GraduateWidget::on_setting_pushButton_clicked()
{
    KW_MAX = ui->maxkw_lineEdit->text().toInt();       //最大功率
    KW_H[1] = ui->washing_lineEdit_1->text().toInt();   //洗衣机
    KW_H[2] = ui->air_lineEdit_1->text().toInt();       //空调
    KW_H[3] = ui->heater_lineEdit_1->text().toInt();    //热水器

    uint a = KW_MAX * KW_H[1] * KW_H[2] * KW_H[3];     //非数字等于0，用于异常输入判断
    if(a==0)
    {
        /*打印消息*/
        time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
        ui->message_textEdit->append(time+QString("设置--请输入正确的数值！\n"));

    }
    else
    {
        /*打印消息*/
        time = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss\n");
        ui->message_textEdit->append(time+QString("设置--功率设置成功！\n"));
    }
}
