#include "graduatewidget.h"
#include <QApplication>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv); //使用main函数参数创建QApplication对象
    GraduateWidget w;           //创建窗口对象，widget继承自QWidget,QWidget 是一个窗口基类

    int currentScreen = a.desktop()->screenNumber(&w);  //程序所在的屏幕编号
    QRect rect = a.desktop()->screenGeometry(currentScreen);//程序所在屏幕的尺寸
    w.move((rect.width()-w.width())/2,(rect.height()-w.height())/2-50);//移动到所在屏幕中间
    w.show();
    return a.exec();            //让程序进入消息循环， 开始事件处理
}
