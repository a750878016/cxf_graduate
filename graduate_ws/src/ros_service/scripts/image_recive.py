#!/usr/bin/env python
#coding:utf-8

import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge,CvBridgeError

def callback(data):
    bridge = CvBridge()                             # 实例CvBridge
    cv_image = bridge.imgmsg_to_cv2(data,"bgr8")    # gbr8格式
    cv2.imshow("lala",cv_image)                     # 显示
    cv2.waitKey(1)

def showImage():
    rospy.init_node('showImage',anonymous = True)   #初始化节点
    rospy.Subscriber('/camera/image_raw', Image, callback)
    rospy.spin()                                    #循环等待

if __name__ == '__main__':
    try:
        showImage()
    except rospy.ROSInterruptException:
        pass

