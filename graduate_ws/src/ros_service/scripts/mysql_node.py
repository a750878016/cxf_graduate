#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy        # Python 版本的 ROS 库 （library）
from ros_service.msg import data    # 自定义消息
import pymysql
import pprint
import time

add_data = []          # 存放将要保存的数据
try:
    # 连接mysql数据库服务
    connc = pymysql.Connect(
        host='localhost',  # mysql服务端ip ，默认：localhost(127.0.0.1)
        user='graduate',  # 用户
        password="123456",  # 密码
        database='graduate_data',  # 数据库
        port=3306,  # 端口号
        charset="utf8",  # 编码
    )

    # 创建游标对象，在连接没有关闭之前，游标对象可以反复使用
    cur = connc.cursor()
    print("mysql connect success!\n")

except Exception as e:
    print(e)
    connc.rollback()    # 数据回滚


def stm32_dataInfoCallback(msg):
    global add_data
    if msg.head_1 == 0xa5 and msg.head_2 == 0x5a and msg.type == 0x02:
        add_data = []
        add_data.append(msg.node)
        add_data.append(msg.device)
        add_data.append(msg.data_1)
        add_data.append(msg.data_2)
        add_data.append(msg.data_3)
        add_data.append(msg.data_4)
        try:
            # 插入数据
            sql1 = "INSERT INTO stm32_data (node,device,data_1,data_2,data_3,data_4) VALUES (%s,%s,%s,%s,%s,%s)"
            cur.execute(sql1, add_data)
            # 与查询不同，因为它要修改数据库的数据，因此这里我们必须要有提交操作
            connc.commit()

        except Exception as e:
            print(e)
            connc.rollback()  # 数据回滚

    elif msg.head_1 == 0xa5 and msg.head_2 == 0x5a and msg.type == 0x03:
        add_data = []
        add_data.append(msg.node)
        add_data.append(msg.device)
        add_data.append(msg.data_1)
        add_data.append(msg.data_2)
        add_data.append(msg.data_3)
        add_data.append(msg.data_4)
        try:
            sql1 = "INSERT INTO qt_control (node,device,data_1,data_2,data_3,data_4) VALUES (%s,%s,%s,%s,%s,%s)"
            cur.execute(sql1, add_data)
            connc.commit()

        except Exception as e:
            print(e)
            connc.rollback()  # 数据回滚


def stm32_data_subscriber():
    # ROS节点初始化
    rospy.init_node('mysql_node', anonymous=True)   # 不太关心节点的唯一性，设置anonymous=True。
    # 创建一个Subscriber，订阅名为/stm_data的topic，类型为data消息，注册回调函数stm32_dataInfoCallback
    rospy.Subscriber("/stm_data", data, stm32_dataInfoCallback)

    # 循环等待回调函数
    rospy.spin()

    # 关闭游标对象
    cur.close()
    # 关闭连接
    connc.close()

if __name__ == '__main__':
    stm32_data_subscriber()

