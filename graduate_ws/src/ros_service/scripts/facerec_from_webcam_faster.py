#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import face_recognition
import cv2
import numpy as np
import os
import rospy
import socket
import time
from ros_service.msg import control    # 自定义消息
IMAGE_WIDTH = 640
IMAGE_HEIGHT = 480


def face_recognition_node():

    video_capture = cv2.VideoCapture(0)  # 获取对网络摄像头#0（默认网络摄像头）的引用

    rospy.init_node('face_recognition_node', anonymous=True)    # ROS节点初始化

    image_save_publisher = rospy.Publisher('/stm_data', control, queue_size=1)    #发布/stm_data话题，保存成功告诉服务器
    servos_publisher = rospy.Publisher('/qt_control', control, queue_size=1)

    rospy.set_param("/save_image", 0);  # 拍照
    rospy.set_param("/door_auto", 0);  # 门

    path = "/home/nano/graduate_ws/src/ros_service/scripts/image"  # 存放照片的目录
    files = os.listdir(path)  # 得到文件夹下的所有文件名称
    file_len = len(files)
    print(file_len)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)    # 创建一个套接字
    addr = ('192.168.4.3', 8099)

    # 创建已知人脸编码及其名称的数组
    known_face_encodings = [

    ]
    known_face_names = [
        
    ]

    for file in files:  # 遍历文件夹
        if file.endswith('.jpg'):  # 判断是不是jpg
            # Load a sample picture and learn how to recognize it.
            image = face_recognition.load_image_file(path + '/' + file)
            try:
                known_face_encodings.append(face_recognition.face_encodings(image)[0])
                known_face_names.append("Known")
            except:
                print("存在无法解码图片，删除！"+path + '/' + file+"\n")
                os.remove(path + '/' + file)


    # 初始化一些变量
    face_locations = []
    face_encodings = []
    face_names = []
    process_this_frame = True
    # 设置循环的频率
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        # 抓取单帧视频
        ret, frame = video_capture.read()   # ret 为True 或 False (480,640,3)

        if(rospy.get_param('/save_image')): # 1表示拍照任务,采集一张图片
            file_len = file_len+1
            cv2.imwrite(path+"/"+str(file_len)+".jpg", frame)
            print("save_image:"+path+"/"+str(file_len)+".jpg")
            image = face_recognition.load_image_file(path+"/"+str(file_len)+".jpg")
            try:
                known_face_encodings.append(face_recognition.face_encodings(image)[0])  # 添加人脸编码，有些时候照片没有完整的人脸就是解码失败，
                known_face_names.append("Known")
                Control = control()
                Control.head_1 = 0xa5;
                Control.head_2 = 0x5a;
                Control.node = 0x05;    # jetson nano
                Control.type = 0x02;    # 数据
                Control.device = 0x06;  # 摄像头
                Control.data_1 = 0x02;  # 采集成功
                Control.data_2 = 0xff;
                Control.data_3 = 0xff;
                Control.data_4 = 0xff;
                Control.len = 0x0c;
                Control.check = 0xff;
                Control.tail_1 = 0x0d;
                Control.tail_2 = 0x0a;
                image_save_publisher.publish(Control)
            except:
                print("照片不合适，请重新采集" + path+"/"+str(file_len)+".jpg" + "\n")
                os.remove(path+"/"+str(file_len)+".jpg")
                file_len = file_len - 1         # 恢复
                Control = control()
                Control.head_1 = 0xa5;
                Control.head_2 = 0x5a;
                Control.node = 0x05;    # jetson nano
                Control.type = 0x02;     # 数据
                Control.device = 0x06;  # 摄像头
                Control.data_1 = 0x03;  # 采集失败
                Control.data_2 = 0xff;
                Control.data_3 = 0xff;
                Control.data_4 = 0xff;
                Control.len = 0x0c;
                Control.check = 0xff;
                Control.tail_1 = 0x0d;
                Control.tail_2 = 0x0a;
                image_save_publisher.publish(Control)

            rospy.set_param("/save_image", 0);  # 设置参数


            

        # 每隔一帧才处理一次视频,以节省时间
        if process_this_frame:
            # 将视频帧调整为 1/4 大小，以加快人脸识别处理速度
            small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

            # 将图像从BGR颜色（OpenCV使用）转换为RGB颜色（face_recognition使用）
            # rgb_small_frame = small_frame[:, :, ::-1]
            # ascontiguousarray函数将一个内存不连续存储的数组转换为内存连续存储的数组，使得运行速度更快。
            rgb_small_frame = np.ascontiguousarray(small_frame[:, :, ::-1])

            # 查找当前视频帧中的所有人脸和人脸编码
            face_locations = face_recognition.face_locations(rgb_small_frame,)
            face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

            face_names = []
            for face_encoding in face_encodings:
                # 查看该面是否与已知面匹配
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
                name = "Unknown"    # 默认为Unknown

                # 或者，使用与新面距离最小的已知面
                face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)

                #print("face_distances：",face_distances)
                best_match_index = np.argmin(face_distances)
                #print("best_match_index：",best_match_index)
                #print("matches:",matches)
                if matches[best_match_index]:
                    name = known_face_names[best_match_index]

                face_names.append(name)

                if name == "Known" and rospy.get_param('/door_auto'):     # 人脸存在
                    Control = control()
                    Control.head_1 = 0xa5;
                    Control.head_2 = 0x5a;
                    Control.node = 0x01;  # 节点一
                    Control.type = 0x01;  # 控制
                    Control.device = 0x02;  # 舵机
                    Control.data_1 = 0x01;  # 90度，开
                    Control.data_2 = 0xff;
                    Control.data_3 = 0xff;
                    Control.data_4 = 0xff;
                    Control.len = 0x0c;
                    Control.check = 0xff;
                    Control.tail_1 = 0x0d;
                    Control.tail_2 = 0x0a;
                    servos_publisher.publish(Control)   #发布


        process_this_frame = not process_this_frame


        # 显示结果
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # 放大人脸位置，因为我们在其中检测到的帧被放大到1/4大小
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4

            # 在脸周围画一个方框
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # 在面下方绘制一个带有名称的标签
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        # 显示生成的图像
        # cv2.imshow('Video', frame)

        _, send_data = cv2.imencode('.jpg', frame, [cv2.IMWRITE_JPEG_QUALITY, 50])    # 压缩图片
        img_encode = np.array(send_data)
        img_data = img_encode.tostring()                                              # 二进制
        s.sendto(img_data, addr)                                                      # 发送

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        # 按照循环频率延时
        rate.sleep()
    # 释放网络摄像头
    video_capture.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    try:
        face_recognition_node()
    except rospy.ROSInterruptException:
        pass
