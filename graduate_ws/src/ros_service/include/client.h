#ifndef CLIENT_H
#define CLIENT_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<iostream>
#include <ros/ros.h>
#include "ros/time.h"

//socket
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

//多线程
#include <thread>					

//消息
#include "ros_service/data.h"		//环境数据消息
#include "ros_service/control.h"	//控制数据消息

#define PROT 		8088			//端口号
//#define SERVER_IP	192.168.4.3	//服务器ip地址
#define BUF_SIZE	16				//长度

using namespace std;

class actuator
{
public:

	actuator(ros::NodeHandle nh);
	~actuator();
	void run();
	
public:

	char * SERVER_IP = "192.168.4.3";
	int socket_ser;
	
	pthread_t tids;				//线程标认符

	//存放数据结构体
	unsigned char send_data[BUF_SIZE];		//发送
	unsigned char reciver_data[BUF_SIZE];	//接收
	
	//消息
	ros_service::data 		Data;			//定义数据模型
	ros_service::control	Control;		//定义控制模型
	
	//发布者
	ros::Publisher qt_control_pub;			//接收QT发送过来的命令，发布出去
	
	//订阅者
	ros::Subscriber stm_data_sub;			//订阅名为/stm_data的topic
	
	//回调函数
	void StmDataCallback(const ros_service::data::ConstPtr& msg);// 接收到订阅的消息后，发送至QT
	
	void socket_recv(void);					//接收函数,发布者发送
};

#endif
