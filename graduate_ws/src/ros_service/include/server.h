#ifndef SERVER_H
#define SERVER_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<iostream>
#include <ros/ros.h>
#include "ros/time.h"

//socket
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

//多线程
#include <thread>					

//消息
#include "ros_service/data.h"		//环境数据消息
#include "ros_service/control.h"	//控制数据消息

#define PROT 		8088			//端口号
//#define SERVER_IP	192.168.1.104	//本地服务器ip地址
#define BUF_SIZE	16				//长度

using namespace std;

class actuator
{
public:

	actuator(ros::NodeHandle nh);
	~actuator();
	void run();

public:

	char * SERVER_IP = "192.168.4.2";
	int socket_fd;
	int fd;
	pthread_t tids;				//线程标认符

	//存放数据结构体
	unsigned char send_data[BUF_SIZE];		//发送
	unsigned char reciver_data[BUF_SIZE];	//接收	
	
	//消息
	ros_service::data 		Data;			//定义数据模型
	ros_service::control	Control;		//定义控制模型
	
	//发布者
	ros::Publisher stm_data_pub;			//接收STM32发送过来的数据，发布出去
	
	//订阅者
	ros::Subscriber qt_control_sub;			//订阅名为/qt_control的topic
	
	//回调函数
	void QtControlCallback(const ros_service::control::ConstPtr& msg);// 接收到订阅的消息后，发送至STM32
	
	void socket_recv(void);					//接收函数,发布者发送
};

#endif