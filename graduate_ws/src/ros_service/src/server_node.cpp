#include "server.h"
int main(int argc, char *argv[])
{
  ros::init(argc, argv, "server_node");
  ros::NodeHandle nh;
  actuator node(nh);
  node.run();
  return 0;
}
