#include "server.h"

actuator::actuator(ros::NodeHandle handle)
{	
	stm_data_pub = handle.advertise<ros_service::data>("/stm_data", 5);	//接收STM32发送过来的数据，并发布出去
    qt_control_sub = handle.subscribe("/qt_control", 10, &actuator::QtControlCallback,this);//订阅名为/qt_control的topic，注册回调函数QtControlCallback
}
actuator::~actuator()
{
}
void actuator::run()
{
	ros::Rate rate(10.0);
	//定义sockfd
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);	//SOCK_STREAM：面向连接的套接字
    if (socket_fd == -1)
    {
        ROS_INFO("socket创建失败！\n");
		return;
    }
    //定义sockaddr_in
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;		//IPv4 网络协议的套接字类型
    addr.sin_port = htons(PROT);	//端口号
	//inet_addr方法可以转化字符串，主要用来将一个十进制的数转化为二进制的数，用途多于ipv4的IP转化。
    addr.sin_addr.s_addr = inet_addr(SERVER_IP);
    
	//3.bind()绑定
    //参数一：0的返回值（socket_fd）
    //参数二：(struct sockaddr*)&addr 前面结构体，即地址
    //参数三: addr结构体的长度
    int res = bind(socket_fd,(struct sockaddr*)&addr,sizeof(addr));
    if (res == -1)
    {
        ROS_INFO("bing创建失败！\n");
		return;
    }
	
    cout << "bind ok 等待客户端的连接..." << endl;
	
    //4.监听客户端listen()函数
    //参数二：进程上限，一般小于30
    listen(socket_fd,30);
	
    //5.等待客户端的连接accept()，返回用于交互的socket描述符
    struct sockaddr_in client;
    socklen_t len = sizeof(client);
    fd = accept(socket_fd,(struct sockaddr*)&client,&len);	//获得绑定标识
    if (fd == -1)
    {
        ROS_INFO("accept错误!\n");
        return;
    }
	
    //6.使用第5步返回socket描述符，进行读写通信。
    char *ip = inet_ntoa(client.sin_addr);	//获取客户端ip地址
    cout << "客户： 【" << ip << "】连接成功!\n" << endl; 
    write(fd, " Welcome to visit ", 17);	//发送回客户端
	
	std::thread thread(&actuator::socket_recv,this);//开线程,接收STM32发送过来的数据，然后发布	
    
    ros::spin();							//循环等待回调函数
	thread.join();
    return;
}

//接受数据函数,转发
void actuator::socket_recv(void)
{
	ros::Rate rate(10.0);
	while(ros::ok())
	{
		int size = read(fd, reciver_data, sizeof(reciver_data));//通过fd与客户端联系在一起,返回接收到的字节数
		
        if (size>=1) //接受到数据进行处理
        {
            if(reciver_data[0]==0xa5&&reciver_data[1]==0x5a)
			{
				Data.head_1 = reciver_data[0];
				Data.head_2 = reciver_data[1];
				Data.node = reciver_data[2];
				Data.type = reciver_data[3];
				Data.device = reciver_data[4];
				Data.data_1 = reciver_data[5];
				Data.data_2 = reciver_data[6];
				Data.data_3 = reciver_data[7];
				Data.data_4 = reciver_data[8];
				Data.len = reciver_data[9];
				Data.check = reciver_data[10];
				Data.tail_1 = reciver_data[11];
				Data.tail_2 |= reciver_data[12];
			}
           	stm_data_pub.publish(Data);
			memset(&reciver_data,0,sizeof(reciver_data));
        }
		rate.sleep();
	}
	//关闭socket_fd
    close(fd);
    close(socket_fd);
}

//订阅者
void actuator::QtControlCallback(const ros_service::control::ConstPtr& msg)				// 接收到订阅的消息后，发送至STM32
{
	memset(&send_data,0,sizeof(send_data));
    send_data[0] = msg->head_1;
	send_data[1] = msg->head_2;
	send_data[2] = msg->node;
	send_data[3] = msg->type;
	send_data[4] = msg->device;
	send_data[5] = msg->data_1;
	send_data[6] = msg->data_2;
	send_data[7] = msg->data_3;
	send_data[8] = msg->data_4;
	send_data[9] = msg->len;
	send_data[10] = msg->check;
	send_data[11] = msg->tail_1;
	send_data[12] = msg->tail_2;
	send(fd,send_data,13,0);
}


