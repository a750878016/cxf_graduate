#include "client.h"

actuator::actuator(ros::NodeHandle handle)
{	
	qt_control_pub = handle.advertise<ros_service::control>("/qt_control", 5);//接收QT发送过来的命令，并发布出去
    stm_data_sub = handle.subscribe("/stm_data", 10, &actuator::StmDataCallback,this);	//订阅名为/stm_data的topic，注册回调函数StmDataCallback
}
actuator::~actuator()
{
}
void actuator::run()
{
	ros::Rate rate(10.0);
	//定义sockfd
	socket_ser = socket(AF_INET,SOCK_STREAM,0);	//SOCK_STREAM：面向连接的套接字
	if(socket_ser<0)
	{
		ROS_INFO("socket创建失败！\n");
		return;
	}	
	//定义sockaddr_in
	struct sockaddr_in sev_addr;
	memset(&sev_addr,0,sizeof(sev_addr));
	sev_addr.sin_family = AF_INET;		//IPv4 网络协议的套接字类型
	sev_addr.sin_port = htons(PROT);
	//inet_addr方法可以转化字符串，主要用来将一个十进制的数转化为二进制的数，用途多于ipv4的IP转化。
	sev_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
	ROS_INFO("socket connecting...\n");
	
	//循环等待连接成功
	while(ros::ok()){
		if(connect(socket_ser,(struct sockaddr*)&sev_addr,sizeof(sev_addr))<0)
			ROS_INFO("......");
		else
			break;
		rate.sleep();
	}
	ROS_INFO("socket connecte success...");
		
	std::thread thread(&actuator::socket_recv,this);//开线程
    
    ros::spin();		// 循环等待回调函数

	thread.join();
    return;
}


//接受数据函数,转发
void actuator::socket_recv(void)
{
	ros::Rate rate(10.0);
	while(ros::ok())
	{
		recv(socket_ser,reciver_data,16,0);
		if(reciver_data[0]==0xa5 && reciver_data[1]==0x5a)			//验证，这里的到的数据类型有0x01,0x02,0x03
		{
			if(reciver_data[2]==0x05)								//节点为jetson nano
			{	
				if(reciver_data[4]==0x06&&reciver_data[3]==0x01)	//设备号为0x06摄像头&&控制拍照
				{
					ros::param::set("/save_image", 1);				//拍照参数,1为表示拍照任务
					memset(&reciver_data,0,sizeof(reciver_data));
					continue;
				}
			}
			if(reciver_data[2]==0x04)							//节点为stm32,代表自动控制
			{
				if(reciver_data[4]==0x06&&reciver_data[5]==0x01)	//自动开门
					ros::param::set("/door_auto", 1);				//拍照参数,1为表示拍照任务
					
				else if(reciver_data[4]==0x06&&reciver_data[5]==0x02)//关闭自动
					ros::param::set("/door_auto", 0);	
			}

			Control.head_1 = reciver_data[0];
			Control.head_2 = reciver_data[1];
			Control.node = reciver_data[2];
			Control.type = reciver_data[3];
			Control.device = reciver_data[4];			
			Control.data_1 = reciver_data[5];
			Control.data_2 = reciver_data[6];
			Control.data_3 = reciver_data[7];
			Control.data_4 = reciver_data[8];
			Control.len = reciver_data[9];
			Control.check = reciver_data[10];
			Control.tail_1 = reciver_data[11];
			Control.tail_2 |= reciver_data[12];
			qt_control_pub.publish(Control);						//发布
			memset(&reciver_data,0,sizeof(reciver_data));
		}
		rate.sleep();
	}
	
	//关闭socket_fd
    close(socket_ser);
	
}

//订阅者
void actuator::StmDataCallback(const ros_service::data::ConstPtr& msg)					// 接收到订阅的消息后，发送至QT
{
	memset(&send_data,0,sizeof(send_data));
    send_data[0] = msg->head_1;
	send_data[1] = msg->head_2;
	send_data[2] = msg->node;
	send_data[3] = msg->type;
	send_data[4] = msg->device;
	send_data[5] = msg->data_1;
	send_data[6] = msg->data_2;
	send_data[7] = msg->data_3;
	send_data[8] = msg->data_4;
	send_data[9] = msg->len;
	send_data[10] = msg->check;
	send_data[11] = msg->tail_1;
	send_data[12] = msg->tail_2;
	send(socket_ser,send_data,13,0);
}
	
