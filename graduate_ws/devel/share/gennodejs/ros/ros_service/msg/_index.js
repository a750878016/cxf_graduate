
"use strict";

let control = require('./control.js');
let data = require('./data.js');

module.exports = {
  control: control,
  data: data,
};
