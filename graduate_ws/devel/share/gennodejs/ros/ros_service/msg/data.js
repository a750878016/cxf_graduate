// Auto-generated. Do not edit!

// (in-package ros_service.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class data {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.head_1 = null;
      this.head_2 = null;
      this.node = null;
      this.type = null;
      this.device = null;
      this.data_1 = null;
      this.data_2 = null;
      this.data_3 = null;
      this.data_4 = null;
      this.len = null;
      this.check = null;
      this.tail_1 = null;
      this.tail_2 = null;
    }
    else {
      if (initObj.hasOwnProperty('head_1')) {
        this.head_1 = initObj.head_1
      }
      else {
        this.head_1 = 0;
      }
      if (initObj.hasOwnProperty('head_2')) {
        this.head_2 = initObj.head_2
      }
      else {
        this.head_2 = 0;
      }
      if (initObj.hasOwnProperty('node')) {
        this.node = initObj.node
      }
      else {
        this.node = 0;
      }
      if (initObj.hasOwnProperty('type')) {
        this.type = initObj.type
      }
      else {
        this.type = 0;
      }
      if (initObj.hasOwnProperty('device')) {
        this.device = initObj.device
      }
      else {
        this.device = 0;
      }
      if (initObj.hasOwnProperty('data_1')) {
        this.data_1 = initObj.data_1
      }
      else {
        this.data_1 = 0;
      }
      if (initObj.hasOwnProperty('data_2')) {
        this.data_2 = initObj.data_2
      }
      else {
        this.data_2 = 0;
      }
      if (initObj.hasOwnProperty('data_3')) {
        this.data_3 = initObj.data_3
      }
      else {
        this.data_3 = 0;
      }
      if (initObj.hasOwnProperty('data_4')) {
        this.data_4 = initObj.data_4
      }
      else {
        this.data_4 = 0;
      }
      if (initObj.hasOwnProperty('len')) {
        this.len = initObj.len
      }
      else {
        this.len = 0;
      }
      if (initObj.hasOwnProperty('check')) {
        this.check = initObj.check
      }
      else {
        this.check = 0;
      }
      if (initObj.hasOwnProperty('tail_1')) {
        this.tail_1 = initObj.tail_1
      }
      else {
        this.tail_1 = 0;
      }
      if (initObj.hasOwnProperty('tail_2')) {
        this.tail_2 = initObj.tail_2
      }
      else {
        this.tail_2 = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type data
    // Serialize message field [head_1]
    bufferOffset = _serializer.uint8(obj.head_1, buffer, bufferOffset);
    // Serialize message field [head_2]
    bufferOffset = _serializer.uint8(obj.head_2, buffer, bufferOffset);
    // Serialize message field [node]
    bufferOffset = _serializer.uint8(obj.node, buffer, bufferOffset);
    // Serialize message field [type]
    bufferOffset = _serializer.uint8(obj.type, buffer, bufferOffset);
    // Serialize message field [device]
    bufferOffset = _serializer.uint8(obj.device, buffer, bufferOffset);
    // Serialize message field [data_1]
    bufferOffset = _serializer.uint8(obj.data_1, buffer, bufferOffset);
    // Serialize message field [data_2]
    bufferOffset = _serializer.uint8(obj.data_2, buffer, bufferOffset);
    // Serialize message field [data_3]
    bufferOffset = _serializer.uint8(obj.data_3, buffer, bufferOffset);
    // Serialize message field [data_4]
    bufferOffset = _serializer.uint8(obj.data_4, buffer, bufferOffset);
    // Serialize message field [len]
    bufferOffset = _serializer.uint8(obj.len, buffer, bufferOffset);
    // Serialize message field [check]
    bufferOffset = _serializer.uint8(obj.check, buffer, bufferOffset);
    // Serialize message field [tail_1]
    bufferOffset = _serializer.uint8(obj.tail_1, buffer, bufferOffset);
    // Serialize message field [tail_2]
    bufferOffset = _serializer.uint8(obj.tail_2, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type data
    let len;
    let data = new data(null);
    // Deserialize message field [head_1]
    data.head_1 = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [head_2]
    data.head_2 = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [node]
    data.node = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [type]
    data.type = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [device]
    data.device = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [data_1]
    data.data_1 = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [data_2]
    data.data_2 = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [data_3]
    data.data_3 = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [data_4]
    data.data_4 = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [len]
    data.len = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [check]
    data.check = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [tail_1]
    data.tail_1 = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [tail_2]
    data.tail_2 = _deserializer.uint8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 13;
  }

  static datatype() {
    // Returns string type for a message object
    return 'ros_service/data';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '0cc8146e667629e5550c82904a667525';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    uint8  head_1
    uint8  head_2
    uint8  node
    uint8  type
    uint8  device
    uint8  data_1
    uint8  data_2
    uint8  data_3
    uint8  data_4
    uint8  len
    uint8  check
    uint8  tail_1
    uint8  tail_2
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new data(null);
    if (msg.head_1 !== undefined) {
      resolved.head_1 = msg.head_1;
    }
    else {
      resolved.head_1 = 0
    }

    if (msg.head_2 !== undefined) {
      resolved.head_2 = msg.head_2;
    }
    else {
      resolved.head_2 = 0
    }

    if (msg.node !== undefined) {
      resolved.node = msg.node;
    }
    else {
      resolved.node = 0
    }

    if (msg.type !== undefined) {
      resolved.type = msg.type;
    }
    else {
      resolved.type = 0
    }

    if (msg.device !== undefined) {
      resolved.device = msg.device;
    }
    else {
      resolved.device = 0
    }

    if (msg.data_1 !== undefined) {
      resolved.data_1 = msg.data_1;
    }
    else {
      resolved.data_1 = 0
    }

    if (msg.data_2 !== undefined) {
      resolved.data_2 = msg.data_2;
    }
    else {
      resolved.data_2 = 0
    }

    if (msg.data_3 !== undefined) {
      resolved.data_3 = msg.data_3;
    }
    else {
      resolved.data_3 = 0
    }

    if (msg.data_4 !== undefined) {
      resolved.data_4 = msg.data_4;
    }
    else {
      resolved.data_4 = 0
    }

    if (msg.len !== undefined) {
      resolved.len = msg.len;
    }
    else {
      resolved.len = 0
    }

    if (msg.check !== undefined) {
      resolved.check = msg.check;
    }
    else {
      resolved.check = 0
    }

    if (msg.tail_1 !== undefined) {
      resolved.tail_1 = msg.tail_1;
    }
    else {
      resolved.tail_1 = 0
    }

    if (msg.tail_2 !== undefined) {
      resolved.tail_2 = msg.tail_2;
    }
    else {
      resolved.tail_2 = 0
    }

    return resolved;
    }
};

module.exports = data;
