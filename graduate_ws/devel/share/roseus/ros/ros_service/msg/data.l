;; Auto-generated. Do not edit!


(when (boundp 'ros_service::data)
  (if (not (find-package "ROS_SERVICE"))
    (make-package "ROS_SERVICE"))
  (shadow 'data (find-package "ROS_SERVICE")))
(unless (find-package "ROS_SERVICE::DATA")
  (make-package "ROS_SERVICE::DATA"))

(in-package "ROS")
;;//! \htmlinclude data.msg.html


(defclass ros_service::data
  :super ros::object
  :slots (_head_1 _head_2 _node _type _device _data_1 _data_2 _data_3 _data_4 _len _check _tail_1 _tail_2 ))

(defmethod ros_service::data
  (:init
   (&key
    ((:head_1 __head_1) 0)
    ((:head_2 __head_2) 0)
    ((:node __node) 0)
    ((:type __type) 0)
    ((:device __device) 0)
    ((:data_1 __data_1) 0)
    ((:data_2 __data_2) 0)
    ((:data_3 __data_3) 0)
    ((:data_4 __data_4) 0)
    ((:len __len) 0)
    ((:check __check) 0)
    ((:tail_1 __tail_1) 0)
    ((:tail_2 __tail_2) 0)
    )
   (send-super :init)
   (setq _head_1 (round __head_1))
   (setq _head_2 (round __head_2))
   (setq _node (round __node))
   (setq _type (round __type))
   (setq _device (round __device))
   (setq _data_1 (round __data_1))
   (setq _data_2 (round __data_2))
   (setq _data_3 (round __data_3))
   (setq _data_4 (round __data_4))
   (setq _len (round __len))
   (setq _check (round __check))
   (setq _tail_1 (round __tail_1))
   (setq _tail_2 (round __tail_2))
   self)
  (:head_1
   (&optional __head_1)
   (if __head_1 (setq _head_1 __head_1)) _head_1)
  (:head_2
   (&optional __head_2)
   (if __head_2 (setq _head_2 __head_2)) _head_2)
  (:node
   (&optional __node)
   (if __node (setq _node __node)) _node)
  (:type
   (&optional __type)
   (if __type (setq _type __type)) _type)
  (:device
   (&optional __device)
   (if __device (setq _device __device)) _device)
  (:data_1
   (&optional __data_1)
   (if __data_1 (setq _data_1 __data_1)) _data_1)
  (:data_2
   (&optional __data_2)
   (if __data_2 (setq _data_2 __data_2)) _data_2)
  (:data_3
   (&optional __data_3)
   (if __data_3 (setq _data_3 __data_3)) _data_3)
  (:data_4
   (&optional __data_4)
   (if __data_4 (setq _data_4 __data_4)) _data_4)
  (:len
   (&optional __len)
   (if __len (setq _len __len)) _len)
  (:check
   (&optional __check)
   (if __check (setq _check __check)) _check)
  (:tail_1
   (&optional __tail_1)
   (if __tail_1 (setq _tail_1 __tail_1)) _tail_1)
  (:tail_2
   (&optional __tail_2)
   (if __tail_2 (setq _tail_2 __tail_2)) _tail_2)
  (:serialization-length
   ()
   (+
    ;; uint8 _head_1
    1
    ;; uint8 _head_2
    1
    ;; uint8 _node
    1
    ;; uint8 _type
    1
    ;; uint8 _device
    1
    ;; uint8 _data_1
    1
    ;; uint8 _data_2
    1
    ;; uint8 _data_3
    1
    ;; uint8 _data_4
    1
    ;; uint8 _len
    1
    ;; uint8 _check
    1
    ;; uint8 _tail_1
    1
    ;; uint8 _tail_2
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _head_1
       (write-byte _head_1 s)
     ;; uint8 _head_2
       (write-byte _head_2 s)
     ;; uint8 _node
       (write-byte _node s)
     ;; uint8 _type
       (write-byte _type s)
     ;; uint8 _device
       (write-byte _device s)
     ;; uint8 _data_1
       (write-byte _data_1 s)
     ;; uint8 _data_2
       (write-byte _data_2 s)
     ;; uint8 _data_3
       (write-byte _data_3 s)
     ;; uint8 _data_4
       (write-byte _data_4 s)
     ;; uint8 _len
       (write-byte _len s)
     ;; uint8 _check
       (write-byte _check s)
     ;; uint8 _tail_1
       (write-byte _tail_1 s)
     ;; uint8 _tail_2
       (write-byte _tail_2 s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _head_1
     (setq _head_1 (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _head_2
     (setq _head_2 (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _node
     (setq _node (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _type
     (setq _type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _device
     (setq _device (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _data_1
     (setq _data_1 (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _data_2
     (setq _data_2 (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _data_3
     (setq _data_3 (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _data_4
     (setq _data_4 (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _len
     (setq _len (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _check
     (setq _check (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _tail_1
     (setq _tail_1 (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _tail_2
     (setq _tail_2 (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get ros_service::data :md5sum-) "0cc8146e667629e5550c82904a667525")
(setf (get ros_service::data :datatype-) "ros_service/data")
(setf (get ros_service::data :definition-)
      "uint8  head_1
uint8  head_2
uint8  node
uint8  type
uint8  device
uint8  data_1
uint8  data_2
uint8  data_3
uint8  data_4
uint8  len
uint8  check
uint8  tail_1
uint8  tail_2

")



(provide :ros_service/data "0cc8146e667629e5550c82904a667525")


