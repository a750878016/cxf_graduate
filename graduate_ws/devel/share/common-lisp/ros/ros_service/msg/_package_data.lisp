(cl:in-package ros_service-msg)
(cl:export '(HEAD_1-VAL
          HEAD_1
          HEAD_2-VAL
          HEAD_2
          NODE-VAL
          NODE
          TYPE-VAL
          TYPE
          DEVICE-VAL
          DEVICE
          DATA_1-VAL
          DATA_1
          DATA_2-VAL
          DATA_2
          DATA_3-VAL
          DATA_3
          DATA_4-VAL
          DATA_4
          LEN-VAL
          LEN
          CHECK-VAL
          CHECK
          TAIL_1-VAL
          TAIL_1
          TAIL_2-VAL
          TAIL_2
))