; Auto-generated. Do not edit!


(cl:in-package ros_service-msg)


;//! \htmlinclude data.msg.html

(cl:defclass <data> (roslisp-msg-protocol:ros-message)
  ((head_1
    :reader head_1
    :initarg :head_1
    :type cl:fixnum
    :initform 0)
   (head_2
    :reader head_2
    :initarg :head_2
    :type cl:fixnum
    :initform 0)
   (node
    :reader node
    :initarg :node
    :type cl:fixnum
    :initform 0)
   (type
    :reader type
    :initarg :type
    :type cl:fixnum
    :initform 0)
   (device
    :reader device
    :initarg :device
    :type cl:fixnum
    :initform 0)
   (data_1
    :reader data_1
    :initarg :data_1
    :type cl:fixnum
    :initform 0)
   (data_2
    :reader data_2
    :initarg :data_2
    :type cl:fixnum
    :initform 0)
   (data_3
    :reader data_3
    :initarg :data_3
    :type cl:fixnum
    :initform 0)
   (data_4
    :reader data_4
    :initarg :data_4
    :type cl:fixnum
    :initform 0)
   (len
    :reader len
    :initarg :len
    :type cl:fixnum
    :initform 0)
   (check
    :reader check
    :initarg :check
    :type cl:fixnum
    :initform 0)
   (tail_1
    :reader tail_1
    :initarg :tail_1
    :type cl:fixnum
    :initform 0)
   (tail_2
    :reader tail_2
    :initarg :tail_2
    :type cl:fixnum
    :initform 0))
)

(cl:defclass data (<data>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <data>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'data)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ros_service-msg:<data> is deprecated: use ros_service-msg:data instead.")))

(cl:ensure-generic-function 'head_1-val :lambda-list '(m))
(cl:defmethod head_1-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:head_1-val is deprecated.  Use ros_service-msg:head_1 instead.")
  (head_1 m))

(cl:ensure-generic-function 'head_2-val :lambda-list '(m))
(cl:defmethod head_2-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:head_2-val is deprecated.  Use ros_service-msg:head_2 instead.")
  (head_2 m))

(cl:ensure-generic-function 'node-val :lambda-list '(m))
(cl:defmethod node-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:node-val is deprecated.  Use ros_service-msg:node instead.")
  (node m))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:type-val is deprecated.  Use ros_service-msg:type instead.")
  (type m))

(cl:ensure-generic-function 'device-val :lambda-list '(m))
(cl:defmethod device-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:device-val is deprecated.  Use ros_service-msg:device instead.")
  (device m))

(cl:ensure-generic-function 'data_1-val :lambda-list '(m))
(cl:defmethod data_1-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:data_1-val is deprecated.  Use ros_service-msg:data_1 instead.")
  (data_1 m))

(cl:ensure-generic-function 'data_2-val :lambda-list '(m))
(cl:defmethod data_2-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:data_2-val is deprecated.  Use ros_service-msg:data_2 instead.")
  (data_2 m))

(cl:ensure-generic-function 'data_3-val :lambda-list '(m))
(cl:defmethod data_3-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:data_3-val is deprecated.  Use ros_service-msg:data_3 instead.")
  (data_3 m))

(cl:ensure-generic-function 'data_4-val :lambda-list '(m))
(cl:defmethod data_4-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:data_4-val is deprecated.  Use ros_service-msg:data_4 instead.")
  (data_4 m))

(cl:ensure-generic-function 'len-val :lambda-list '(m))
(cl:defmethod len-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:len-val is deprecated.  Use ros_service-msg:len instead.")
  (len m))

(cl:ensure-generic-function 'check-val :lambda-list '(m))
(cl:defmethod check-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:check-val is deprecated.  Use ros_service-msg:check instead.")
  (check m))

(cl:ensure-generic-function 'tail_1-val :lambda-list '(m))
(cl:defmethod tail_1-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:tail_1-val is deprecated.  Use ros_service-msg:tail_1 instead.")
  (tail_1 m))

(cl:ensure-generic-function 'tail_2-val :lambda-list '(m))
(cl:defmethod tail_2-val ((m <data>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ros_service-msg:tail_2-val is deprecated.  Use ros_service-msg:tail_2 instead.")
  (tail_2 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <data>) ostream)
  "Serializes a message object of type '<data>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'head_1)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'head_2)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'node)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'type)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'device)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data_1)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data_2)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data_3)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data_4)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'len)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'check)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'tail_1)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'tail_2)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <data>) istream)
  "Deserializes a message object of type '<data>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'head_1)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'head_2)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'node)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'type)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'device)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data_1)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data_2)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data_3)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data_4)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'len)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'check)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'tail_1)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'tail_2)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<data>)))
  "Returns string type for a message object of type '<data>"
  "ros_service/data")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'data)))
  "Returns string type for a message object of type 'data"
  "ros_service/data")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<data>)))
  "Returns md5sum for a message object of type '<data>"
  "0cc8146e667629e5550c82904a667525")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'data)))
  "Returns md5sum for a message object of type 'data"
  "0cc8146e667629e5550c82904a667525")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<data>)))
  "Returns full string definition for message of type '<data>"
  (cl:format cl:nil "uint8  head_1~%uint8  head_2~%uint8  node~%uint8  type~%uint8  device~%uint8  data_1~%uint8  data_2~%uint8  data_3~%uint8  data_4~%uint8  len~%uint8  check~%uint8  tail_1~%uint8  tail_2~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'data)))
  "Returns full string definition for message of type 'data"
  (cl:format cl:nil "uint8  head_1~%uint8  head_2~%uint8  node~%uint8  type~%uint8  device~%uint8  data_1~%uint8  data_2~%uint8  data_3~%uint8  data_4~%uint8  len~%uint8  check~%uint8  tail_1~%uint8  tail_2~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <data>))
  (cl:+ 0
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <data>))
  "Converts a ROS message object to a list"
  (cl:list 'data
    (cl:cons ':head_1 (head_1 msg))
    (cl:cons ':head_2 (head_2 msg))
    (cl:cons ':node (node msg))
    (cl:cons ':type (type msg))
    (cl:cons ':device (device msg))
    (cl:cons ':data_1 (data_1 msg))
    (cl:cons ':data_2 (data_2 msg))
    (cl:cons ':data_3 (data_3 msg))
    (cl:cons ':data_4 (data_4 msg))
    (cl:cons ':len (len msg))
    (cl:cons ':check (check msg))
    (cl:cons ':tail_1 (tail_1 msg))
    (cl:cons ':tail_2 (tail_2 msg))
))
