
(cl:in-package :asdf)

(defsystem "ros_service-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "control" :depends-on ("_package_control"))
    (:file "_package_control" :depends-on ("_package"))
    (:file "data" :depends-on ("_package_data"))
    (:file "_package_data" :depends-on ("_package"))
  ))